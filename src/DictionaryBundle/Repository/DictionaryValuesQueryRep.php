<?php

namespace DictionaryBundle\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DictionaryValuesQueryRep extends Controller
{

    private $em;
    private $conn;

    public function __construct(EntityManager $em, Connection $conn)
    {
        $this->em = $em;
        $this->conn = $conn;
    }

    public function getAllValues($dict_id)
    {
        $values = $this->em->getRepository('DictionaryBundle:DictValues');
        $result = $values->findByDictId($dict_id);
        return $result;
    }

    public function getValue($dict_id, $id)
    {
        $values = $this->em->getRepository('DictionaryBundle:DictValues');
        $result = $values->findOneBy(
            array('id' => $id, 'dictId' => $dict_id)
        );
        return $result;
    }

    public function getValuesByDictId($dict_id)
    {
        $query = 'SELECT dv.value, dv.id 
        FROM dict_values AS dv
        WHERE dict_id = '.$dict_id;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        return $result;
    }
}