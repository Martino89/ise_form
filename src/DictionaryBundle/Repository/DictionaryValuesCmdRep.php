<?php

namespace DictionaryBundle\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use DictionaryBundle\Entity\DictValues;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DictionaryValuesCmdRep extends Controller
{
    private $em;
    private $conn;

    public function __construct(EntityManager $em, Connection $conn)
    {
        $this->em = $em;
        $this->conn = $conn;
    }

    public function deleteValue($dict_id, $id)
    {
        $values = $this->em->getRepository('DictionaryBundle:DictValues');
        $result = $values->findOneBy(
            array('id' => $id, 'dictId' => $dict_id)
        );
        $this->em->remove($result);
        $this->em->flush();

        return $result;
    }

    public function updateValue($dict_id, $id, $value)
    {
        $values = $this->em->getRepository('DictionaryBundle:DictValues');
        $dict_value = $values->findOneBy(
            array('id' => $id, 'dictId' => $dict_id)
        );
        $dict_value->setValue($value);
        $this->em->flush();

        return $value;
    }

    public function addValue($dict_id, $value)
    {
        $query = 'SELECT MAX(id) FROM dict_values WHERE dict_id = '.$dict_id.' ';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $max = (int)$stmt->fetchColumn();
        $max++;

        $query = 'INSERT INTO dict_values (id ,dict_id, value) VALUES ('.$max.', '.$dict_id.', "'.$value.'"); ';
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute();
        return $result;
    }
}