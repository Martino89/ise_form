<?php

namespace DictionaryBundle\Repository;

use DictionaryBundle\Entity\DictValues;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ImportCmdRep extends Controller
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function importValues($dict_id, $values, $id_col, $value_col)
    {
        $batchSize = 20;
        foreach ($values as $key => $item) {
            $value = new DictValues();
            $value->setId($item[$id_col]);
            $value->setValue($item[$value_col]);
            $value->setDictId($dict_id);
            $this->em->persist($value);
            if (($key % $batchSize) == 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }
        $this->em->flush();
        $this->em->clear();
        $result = 'Success! Table imported into bundle';
        return $result;
    }

}