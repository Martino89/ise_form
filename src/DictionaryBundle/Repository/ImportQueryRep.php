<?php

namespace DictionaryBundle\Repository;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ImportQueryRep extends Controller
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getValues($table, $id_col, $value_col)
    {
        $query = 'SELECT ' . $id_col . ', ' . $value_col . ' FROM ' . $table . ' ';
        $stm = $this->em->getConnection()->prepare($query);
        $stm->execute();
        $result = $stm->fetchAll();
        return $result;
    }

    public function getTablesList() {
        $sm = $this->em->getConnection()->getSchemaManager();
        $result =  $sm->listTableNames();
        return $result;
    }

}