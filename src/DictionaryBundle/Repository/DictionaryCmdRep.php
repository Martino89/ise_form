<?php

namespace DictionaryBundle\Repository;

use DictionaryBundle\Entity\Dict;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DictionaryCmdRep extends Controller
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function deleteDict($id)
    {
        $dictionaries = $this->em->getRepository('DictionaryBundle:Dict');
        $result = $dictionaries->findOneById($id);
        $this->em->remove($result);
        $this->em->flush();
        return $result;
    }

    public function updateDict($id, $name)
    {
        $dictionaries = $this->em->getRepository('DictionaryBundle:Dict');
        $result = $dictionaries->findOneById($id);
        $result->setName($name);
        $result->setAlias($name);
        $this->em->flush();
        return $result;
    }

    public function addDict($name)
    {
        $dict = new Dict();
        $dict->setName($name);
        $dict->setAlias($name);
        $this->em->persist($dict);
        $this->em->flush();
        return $dict;
    }
}