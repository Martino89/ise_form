<?php

namespace DictionaryBundle\Repository;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InitCmdRep extends Controller
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function createTables($restart)
    {
        if ($restart) {
            $query = 'DROP TABLE IF EXISTS dict, dict_values';
            $this->em->getConnection()->prepare($query)->execute()->fetchAll();
        }
        $sm = $this->em->getConnection()->getSchemaManager();
        if ($sm->tablesExist(['dict', 'dict_values']) == false) {
            $query =
                'CREATE TABLE `dict` (
              `id` INT(11) NOT NULL,
              `alias` VARCHAR(250) COLLATE utf8_polish_ci NOT NULL,
              `name` VARCHAR(250) COLLATE utf8_polish_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

            CREATE TABLE `dict_values` (
              `id` INT(11) NOT NULL,
              `dict_id` INT(11) NOT NULL,
              `value` VARCHAR(512) COLLATE utf8_polish_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

            ALTER TABLE `dict`
              ADD PRIMARY KEY (`id`);
              
            ALTER TABLE dict
              ADD UNIQUE (`alias`);

            ALTER TABLE `dict_values`
              ADD PRIMARY KEY (`id`,`dict_id`);

            ALTER TABLE `dict`
              MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;

            ALTER TABLE `dict_values`
              MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;';
            $this->em->getConnection()->prepare($query)->execute();
            $result = "Success! Bundle tables created.";
        } else {
            $result = "Failure! Tables already exist.";
        }
        return $result;
    }

}