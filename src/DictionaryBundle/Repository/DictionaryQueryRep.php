<?php

namespace DictionaryBundle\Repository;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DictionaryQueryRep extends Controller
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAllDicts()
    {
        $dictionaries = $this->em->getRepository('DictionaryBundle:Dict');
        $result = $dictionaries->findAll();
        return $result;
    }

    public function getDict($name)
    {
        $dictionaries = $this->em->getRepository('DictionaryBundle:Dict');
        $result = $dictionaries->findOneByAlias($name);
        return $result;
    }
}