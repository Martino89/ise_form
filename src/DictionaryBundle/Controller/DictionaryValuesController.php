<?php

namespace DictionaryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/dictionary")
 */

class DictionaryValuesController extends Controller
{
    /**
     * @Route("/{dict}/value-add", options = { "expose" = true }, name="dictionary_value_add")
     */
    public function addAction($dict, Request $request)
    {
        $value = $request->request->get('value');
        $result = $this->get('dictionary.dictionary_values_cmd_srv')->addValue($dict, $value);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/{dict}/get-all", options = { "expose" = true }, name="dictionary_value_get_all")
     */
    public function getAllAction($dict)
    {
        $result = $this->get('dictionary.dictionary_values_query_srv')->getAllValues($dict);
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->serialize($result, 'json');
        $response = new Response($result);
        return $response;
    }

    /**
     * @Route("/{dict}/update", options = { "expose" = true }, name="dictionary_value_update")
     */
    public function updateAction($dict, Request $request)
    {
        $id = $request->request->get('id');
        $value = $request->request->get('value');
        $result = $this->get('dictionary.dictionary_values_cmd_srv')->updateValue($dict, $id, $value);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/{dict}/delete", options = { "expose" = true }, name="dictionary_value_delete")
     */
    public function deleteAction($dict, Request $request)
    {
        $id = $request->request->get('id');
        $result = $this->get('dictionary.dictionary_values_cmd_srv')->deleteValue($dict, $id);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/{dict}/get", options = { "expose" = true }, name="dictionary_value_get")
     */
    public function getAction($dict, Request $request)
    {
        $id = $request->request->get('id');
        $result = $this->get('dictionary.dictionary_values_query_srv')->getValue($dict, $id);
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->serialize($result, 'json');
        $response = new Response($result);
        dump($response);
        return $response;
    }

}
