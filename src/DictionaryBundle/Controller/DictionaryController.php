<?php

namespace DictionaryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/dictionary")
 */
class DictionaryController extends Controller
{
    /**
     * @Route("/manager", options = { "expose" = true }, name="dictionary_manager")
     *
     */
    public function managerAction()
    {
        return $this->render('@Dictionary/index.html.twig');
    }

    /**
     * @Route("/add", options = { "expose" = true }, name="dictionary_add")
     */
    public function addAction(Request $request)
    {
        $result = $this->get('dictionary.dictionary_cmd_srv')->addDict($request);
        dump($result);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/get-all", options = { "expose" = true },  name="dictionary_get_all")
     */
    public function getAllAction()
    {
        $result = $this->get('dictionary.dictionary_query_srv')->getAllDicts();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->serialize($result, 'json');
        $response = new Response($result);
        return $response;
    }

    /**
     * @Route("/update", options = { "expose" = true }, name="dictionary_update")
     */
    public function updateAction(Request $request)
    {
        $result = $this->get('dictionary.dictionary_cmd_srv')->updateDict($request[0], $request[1]);
        dump($result);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/delete", options = { "expose" = true }, name="dictionary_delete")
     */
    public function deleteAction(Request $request)
    {
        $result = $this->get('dictionary.dictionary_cmd_srv')->deleteDict($request);
        dump($result);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/get", options = { "expose" = true }, name="dictionary_get")
     */
    public function getAction(Request $request)
    {
        $result = $this->get('dictionary.dictionary_query_srv')->getDict($request);
        dump($result);
        $response = new JsonResponse($result);
        return $response;
    }

}
