<?php

namespace DictionaryBundle\Services;


use DictionaryBundle\Repository\InitCmdRep;

class InitCmdSrv
{
    private $rep;

    public function __construct(InitCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function  createTables($restart)
    {
        $result =$this->rep->createTables($restart);
        return $result;
    }
}