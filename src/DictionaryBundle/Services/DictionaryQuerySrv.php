<?php

namespace DictionaryBundle\Services;


use DictionaryBundle\Repository\DictionaryQueryRep;

class DictionaryQuerySrv
{
    private $rep;

    public function __construct(DictionaryQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getAllDicts()
    {
        $result = $this->rep->getAllDicts();
        return $result;
    }

    public function getDict($name)
    {
        $result = $this->rep->getDict($name);
        return $result;
    }


}