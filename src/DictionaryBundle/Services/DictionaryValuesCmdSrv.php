<?php

namespace DictionaryBundle\Services;

use DictionaryBundle\Services\DictionaryQuerySrv;
use DictionaryBundle\Repository\DictionaryValuesCmdRep;

class DictionaryValuesCmdSrv
{
    private $rep;
    
    private $srv;

    public function __construct(DictionaryValuesCmdRep $rep, DictionaryQuerySrv $srv)
    {
        $this->rep = $rep;
        $this->srv = $srv;
    }

    public function deleteValue($dict_name, $id)
    {
        $dict = $this->srv->getDict($dict_name);
        $dict_id = $dict->getId();
        $result = $this->rep->deleteValue($dict_id, $id);
        return $result;
    }

    public function updateValue($dict_name, $value, $id)
    {
        $dict = $this->srv->getDict($dict_name);
        $dict_id = $dict->getId();
        $result = $this->rep->updateValue($dict_id, $value, $id);
        return $result;
    }

    public function addValue($dict_name, $value)
    {
        $dict = $this->srv->getDict($dict_name);
        $dict_id = $dict->getId();
        $result = $this->rep->addValue($dict_id, $value);
        return $result;
    }
}