<?php

namespace DictionaryBundle\Services;


use DictionaryBundle\Repository\DictionaryCmdRep;

class DictionaryCmdSrv
{
    private $rep;

    public function __construct(DictionaryCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function deleteDict($id)
    {
        $result = $this->rep->deleteDict($id);
        return $result;
    }

    public function updateDict($id, $name)
    {
        $result = $this->rep->updateDict($id, $name);
        return $result;
    }

    public function addDict($name)
    {
        $result = $this->rep->addDict($name);
        return $result;
    }
}