<?php

namespace DictionaryBundle\Services;

use DictionaryBundle\Services\DictionaryCmdSrv;
use DictionaryBundle\Services\ImportQuerySrv;
use DictionaryBundle\Repository\ImportCmdRep;

class ImportCmdSrv
{
    private $rep;

    private $imp;

    private $dic;

    public function __construct(ImportCmdRep $rep, ImportQuerySrv $imp, DictionaryCmdSrv $dic)
    {
        $this->rep = $rep;
        $this->imp = $imp;
        $this->dic = $dic;
    }

    public function importTable($dict_name, $table, $id_col, $value_col)
    {
        $dict = $this->dic->addDict($dict_name);
        $dict_id = $dict->getId();
        $values = $this->imp->getValues($table, $id_col, $value_col);
        $result = $this->rep->importValues($dict_id, $values, $id_col, $value_col);
        return $result;
    }

}