<?php

namespace DictionaryBundle\Services;

use DictionaryBundle\Services\DictionaryQuerySrv;
use DictionaryBundle\Repository\DictionaryValuesQueryRep;

class DictionaryValuesQuerySrv
{
    private $rep;

    private $srv;

    public function __construct(DictionaryValuesQueryRep $rep, DictionaryQuerySrv $srv)
    {
        $this->rep = $rep;
        $this->srv = $srv;
    }

    public function getAllValues($dict_name)
    {
        $dict = $this->srv->getDict($dict_name);
        $dict_id = $dict->getId();
        $result = $this->rep->getAllValues($dict_id);
        return $result;
    }

    public function getValue($dict_name, $id)
    {
        $dict = $this->srv->getDict($dict_name);
        $dict_id = $dict->getId();
        $result = $this->rep->getValue($dict_id, $id);
        return $result;
    }

    public function getValuesByDictSlag($dict_slag)
    {
        $dict = $this->srv->getDict($dict_slag);
        $dict_id = $dict->getId();
        $result = $this->rep->getValuesByDictId($dict_id);
        return $result;
    }

}