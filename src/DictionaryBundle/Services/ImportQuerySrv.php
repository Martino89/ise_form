<?php

namespace DictionaryBundle\Services;


use DictionaryBundle\Repository\ImportQueryRep;

class ImportQuerySrv
{
    private $rep;

    public function __construct(ImportQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function  getValues($table, $id_col, $value_col)
    {
        $result = $this->rep->getValues($table, $id_col, $value_col);
        return $result;
    }

    public function getTablesList()
    {
        $result = $this->rep->getTablesList();
        return $result;
    }

}