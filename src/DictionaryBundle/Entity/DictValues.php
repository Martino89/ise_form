<?php

namespace DictionaryBundle\Entity;

/**
 * DictValues
 */
class DictValues
{
    /**
     * @var string
     */
    private $value;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dictId;


    /**
     * Set value
     *
     * @param string $value
     *
     * @return DictValues
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return DictValues
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dictId
     *
     * @param integer $dictId
     *
     * @return DictValues
     */
    public function setDictId($dictId)
    {
        $this->dictId = $dictId;

        return $this;
    }

    /**
     * Get dictId
     *
     * @return integer
     */
    public function getDictId()
    {
        return $this->dictId;
    }
}
