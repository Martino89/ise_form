<?php

namespace DictionaryBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class InitCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dictionary:init')
            ->setDescription('Create bundle tables.')
            ->setHelp('This allows you to create bundle tables.')
            ->addOption('reset', 'r', InputOption::VALUE_NONE, 'Drop bundle tables if they exist.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('------------------------------------------------------------');
        $reset = false;
        if ($input->getOption('reset')) {
            $reset = true;
            $output->writeln('Option: reset - bundle tables will be dropped if they exist.');
        }
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            'Do you want to create bundle tables? : ',
            false,
            '/^(y|j)/i'
        );
        if ($helper->ask($input, $output, $question)) {
            $init = $this->getContainer()->get('dictionary.init_cmd_srv');
            $result = $init->createTables($reset);
            $output->write($result);
        } else {
            $output->write('Command "dictionary:init" stopped.');
        }
    }
}

