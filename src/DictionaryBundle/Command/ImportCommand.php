<?php

namespace DictionaryBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dictionary:import')
            ->setDescription('Import table to bundle.')
            ->setHelp('This command allows you to import project tables in to bundle tables.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('------------------------------------------------------------');
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            'Do you want to import tables to bundle? : ',
            false,
            '/^(y|j)/i'
        );
        if ($helper->ask($input, $output, $question)) {
            $dict_name = $helper->ask($input, $output, new Question('Create new dictionary : ', 'Name'));
            $table = $helper->ask($input, $output, new Question('Enter table you want to import: ', 'Table'));
            $id_col = $helper->ask($input, $output, new Question('Table "id" column: ', 'Id'));
            $value_col = $helper->ask($input, $output, new Question('Table "value" column: ', 'Value'));
            $import = $this->getContainer()->get('dictionary.import_cmd_srv');
            $result = $import->importTable($dict_name, $table, $id_col, $value_col);
            $output->write($result);
        } else {
            $output->write('Command "dictionary:import" stopped.');
        }
    }

}

