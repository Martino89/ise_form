function getDictionaries(route, route_value) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: route,
        success: function (responnse) {
            var dictionary_select = $('#dictionary-select');
            dictionary_select.html('');
            var dictionary_table = $('#dictionary-table');
            dictionary_table.html('');
            $.each(responnse, function (index, value) {
                dictionary_select.append('<option value="' + value.alias + '">' + value.name + '</option>');
                dictionary_table.append('<tr><td>' + value.id + '</td><td>' + value.name + '</td>' +
                    '<td><button class="btn btn-primary" name="edit-dictionary" value="' + value.id + '"><span class="glyphicon glyphicon-edit"></span></button>' +
                    '<button class="btn btn-danger" name="remove-dictionary" value="' + value.id + '"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
            });
            getValues(route_value);
        }
    });
}

function getValues(route) {
    var dict = $('#dictionary-select').val();
    var r_route = route.replace("placeholder", dict);
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: r_route,
        success: function (responnse) {
            var value_table = $('#value-table');
            value_table.html('');
            $.each(responnse, function (index, value) {
                value_table.append('<tr><td>' + value.id + '</td><td>' + value.value + '</td>' +
                    '<td><button class="btn btn-primary" name="edit-value" value="' + value.id + '"><span class="glyphicon glyphicon-edit"></span></button>' +
                    '<button class="btn btn-danger" name="remove-value" value="' + value.id + '"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
            });
        }
    });
}

$(document).on('change', '#dictionary-select', function () {
    getValues(val_get_all)
});


getDictionaries(dict_get_all, val_get_all);




