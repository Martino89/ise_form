<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class IseTrainingProposalsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //->add('trpDate')

        $builder
            ->add('trpFacName',TextType::class,array(
                'constraints' => array( new NotBlank()),
                'label' => 'Nazwa placówki',
                'attr' => array('class' => 'form-control')
            ))
            ->add('trpFacTown',TextType::class,array('label' => 'Miejscowość','attr' => array('class' => 'form-control')))
            ->add('trpFacAddress',TextType::class,array('label' => 'Ulica i numer domu','attr' => array('class' => 'form-control')))
            ->add('trpTraName',TextType::class,array('label' => 'Nazwa','attr' => array('class' => 'form-control')))
            ->add('trtId',ChoiceType::class,array('label' => 'Rodzaj',
                'choices'  => $options['types'],
                'attr' => array('class' => 'form-control ')))
            ->add('trpTraSchoolyear',TextType::class,array('label' => 'Rok szkolny','attr' => array('class' => 'form-control')))
            ->add('trpTraHours',IntegerType::class,array('label' => 'Liczba godzin',
                'attr' => array('class' => 'form-control')
                ))
            ->add('trpTraDescription',TextareaType::class,array('label' => 'Uwagi','attr' => array('class' => 'form-control')))
            ->add('trpAutName',TextType::class,array('label' => 'Imię i nazwisko','attr' => array('class' => 'form-control')))
            ->add('trpAutEmail',EmailType::class,array('label' => 'E-mail','attr' => array('class' => 'form-control')))
            ->add('subject',CollectionType::class,array('label' => false,
                'entry_type' => IseSubjectsType::class,
                'entry_options' => ['subjects' => $options['subjects'], 'label' => false]))
            ->add('submit',SubmitType::class,array('attr' => array('class' => 'btn btn-primary')));

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\IseTrainingProposals',
            'subjects' => null,
            'types' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_isetrainingproposals';
    }


}
