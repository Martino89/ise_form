<?php

namespace AppBundle\Form;

use AppBundle\Entity\IseSubjects;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class IseCourseProposalsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('coprFacName', TextType::class, ['label' => 'Nazwa uczelni *',
            'attr' =>['class' => 'form-control']])
            ->add('coprFacTown',TextType::class, ['label' => 'Miejscowość uczelni *',
        'attr' =>['class' => 'form-control']])
            ->add('coprRespFacName',TextType::class, ['label' => 'Nazwa jednostki *',
        'attr' =>['class' => 'form-control']])
            ->add('coprRespFacTown',TextType::class, ['label' => 'Miejscowość jednostki *',
        'attr' =>['class' => 'form-control']])
            ->add('coprRespFacAddress',TextType::class, ['label' => 'Ulica i numer domu jednostki *',
        'attr' =>['class' => 'form-control']])
            ->add('coprRespFacPhone',IntegerType::class, [
                'label' => 'Numer telefonu jednostki',
                'required' => false,
        'attr' =>['class' => 'form-control']])
            ->add('coprRespFacWww',TextType::class, ['label' => 'Adres WWW jednostki',
                'required' => false,
        'attr' =>['class' => 'form-control']])
            ->add('coprRespFacEmail',EmailType::class, ['label' => 'Adres e-mail jednostki',
                'required' => false,
        'attr' =>['class' => 'form-control']])
//            ->add('coprDate')
            ->add('coprCouName',TextType::class, ['label' => 'Nazwa *',
        'attr' =>['class' => 'form-control']])
            ->add('cotId',ChoiceType::class, ['label' => 'Rodzaj *',
        'attr' =>['class' => 'form-control'],
            'choices' => $options['methods']
                ])
            ->add('colId',ChoiceType::class, ['label' => 'Stopień *',
        'attr' =>['class' => 'form-control'],
                'choices' => $options['degree']])
            ->add('coqId',ChoiceType::class, ['label' => 'Uzyskane kwalifikacje *',
        'attr' =>['class' => 'form-control'],
                'choices' => $options['qualifications']])
            ->add('coaId',ChoiceType::class, ['label' => 'Sposób rekrutacji *',
        'attr' =>['class' => 'form-control'],
                'choices' => $options['courses_type']])
            ->add('subjects', CollectionType::class, ['label' => false,
                'entry_type'=>IseSubjectsType::class,
                'entry_options' => [ 'subjects' => $options['subjects_list'],
                    'label' => false]
            ])
            ->add('coprCouAcademicyear',TextType::class, ['label' => 'Rok akademicki (dd-mm-rrrr)/(dd-mm-rrrr)*',
        'attr' =>['class' => 'form-control']])
            ->add('coprCopBeginTimestamp',TextType::class, ['label' => 'Okres trwania, Od (dd-mm-rrrr)',
                'required' => false,
        'attr' =>['class' => 'form-control']])
            ->add('coprCopEndTimestamp',TextType::class, ['label' => 'Do (dd-mm-rrrr)',
                'required' => false,
        'attr' =>['class' => 'form-control']])
            ->add('coprCouSemesters',IntegerType::class, ['label' => 'Liczba semestrów *',
        'attr' =>['class' => 'form-control']])
            ->add('coprCouDescription',TextareaType::class, ['label' => 'Uwagi',
                'required' => false,
        'attr' =>['class' => 'form-control']])
            ->add('coprAutName',TextType::class, ['label' => 'Imie i nazwisko *',
        'attr' =>['class' => 'form-control']])
            ->add('coprAutEmail',EmailType::class, ['label' => 'E-mail *',
        'attr' =>['class' => 'form-control']])

            ->add('submit',SubmitType::class, ['label' => 'Złóż wniosek',
                'attr' =>['class' => 'btn btn-default']])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\IseCourseProposals',
            'subjects_list' => null,
            'courses_type' => null,
            'methods' => null,
            'qualifications' => null,
            'degree' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_isecourseproposals';
    }


}
