function getDictionaries() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: Routing.generate('dictionary_get_all'),
        success: function (responnse) {
            var dictionary_select = $('#dictionary-select');
            dictionary_select.html('');
            $.each(responnse, function (index, value) {
                dictionary_select.append('<option value="' + value.alias + '">' + value.name + '</option>');
            });
            getValues();
        }
    });
}

function getValues() {
    var dict = $('#dictionary-select').val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: Routing.generate('dictionary_value_get_all', {'dict': dict}),
        success: function (response) {
            var value_table = $('#value-table');
            value_table.html('');
            $.each(response, function (index, value) {
                value_table.append('<tr><td>' + value.id + '</td><td>' + value.value + '</td>' +
                    '<td><button class="btn btn-primary" data-toggle="modal" data-target="#editModal" name="edit-value" value="' + value.id + '"><span class="glyphicon glyphicon-edit"></span></button>' +
                    '<button class="btn btn-danger" name="delete-value" value="' + value.id + '"><span class="glyphicon glyphicon-trash"></span></button></td></tr>');
            });
            resetSort();
        }
    });
}

function deleteValue(id) {
    var dict = $('#dictionary-select').val();
    $.ajax({
        type: "POST",
        url: Routing.generate('dictionary_value_delete', {'dict': dict}),
        data: {id: id},
        success: function (response) {
            getValues();
        }
    });
}

function updateValue() {
    var dict = $('#dictionary-select').val();
    $.ajax({
        type: "POST",
        url: Routing.generate('dictionary_value_update', {'dict': dict}),
        data: $('#update-form').serialize(),
        success: function (response) {
            getValues();
        }
    });
}

function addValue() {
    var dict = $('#dictionary-select').val();
    $.ajax({
        type: "POST",
        url: Routing.generate('dictionary_value_add', {'dict': dict}),
        data: $('#add-form').serialize(),
        success: function (response) {
            getValues();
        }
    });
}


function getValue(id) {
    var dict = $('#dictionary-select').val();
    $.ajax({
        type: "POST",
        url: Routing.generate('dictionary_value_get', {'dict': dict}),
        data: {id: id},
        dataType: 'json',
        success: function (value) {
            $('#update-id').val(value.id);
            $('#update-value').val(value.value);
        }
    });
}

function resetSort() {
    $("#table").trigger('updateAll', true);
}

$('#table').tablesorter({
    usNumberFormat: false,
    sortReset: true,
    sortRestart: true
});


$(document).on('change', '#dictionary-select', function () {
    getValues();
});

$(document).on('click', "button[name='delete-value']", function () {
    var id = $(this).val();
    deleteValue(id);
});

$(document).on('click', "button[name='edit-value']", function () {
    var id = $(this).val();
    getValue(id);
});

$(document).on('click', "button[name='update-value']", function () {
    updateValue();
});

$(document).on('click', "button[name='add-value']", function () {
    addValue();
});


getDictionaries();




