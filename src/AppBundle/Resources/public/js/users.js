function getAllUsers() {
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: Routing.generate('admin_users_get_all'),
        success: function (response) {
            var users_table = $('#users-table');
            users_table.html('');
            $.each(response, function (index, user) {
                users_table.append('<tr><td class="col-md-1">' + user.id + '</td><td class="col-md-2">' + user.username +
                    '</td><td class="col-md-3">' + user.email + '</td><td class="col-md-4">' +
                    '<button type="submit" data-toggle="modal" data-target="#updateModal" title="Edytuj użytkownika" ' +
                    'class="btn btn-primary" value="' + user.id + '" ' +
                    'name="update-user-form"><span class="glyphicon glyphicon-edit"></span></button>' +
                    '<button type="submit" title="Usuń użytkownika" class="btn btn-danger" value="' + user.id + '" ' +
                    'name="delete-user"><span class="glyphicon glyphicon-trash"></span></button></td></tr>')
            });
        }
    });
}

$(document).on("click", "button[name='update-user-form']", function () {
    var user_id = this.value;
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: Routing.generate('admin_user_get', {user_id: user_id}),
        success: function (user) {
            $('#update-id').val(user.id);
            $('#update-username').val(user.username);
            $('#update-email').val(user.email);
        }
    });
});

$(document).on("click", "button[name='delete-user']", function () {
    if (!confirm('Czy na pewno chcesz usunąć użytkownika?'))return;
    var user_id = this.value;
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: Routing.generate('admin_user_delete', {user_id: user_id}),
        success: function () {
            getAllUsers();
        }
    });
});

$(document).on("click", "button[name='add-user']", function () {
    var url = Routing.generate('admin_user_add');
    $.ajax({
        type: "POST",
        url: url,
        data: $("#add_user_form").serialize(),
        success: function () {
            document.getElementById("add_user_form").reset();
            getAllUsers();
        }
    });
});

$(document).on("click", "button[name='update-user']", function () {
    var url = Routing.generate('admin_user_update');
    $.ajax({
        type: "POST",
        url: url,
        data: $("#update_user_form").serialize(),
        success: function () {
            getAllUsers();
        }
    });
});


getAllUsers();