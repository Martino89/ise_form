var app = new Vue({
    delimiters: ['${', '}'],
    el: '#app',
    data: {
        results: []
    },
    methods:{
        deleteProposal:function (element) {
            if(!confirm('Czy na pewno chcesz usunąć wniosek?'))
            {
                return
            }

            var url = Routing.generate('admin_trainings_proposals_delete');

            $.post( url, { element: element })
                .done(function( data ) {

                    var url = Routing.generate('admin_trainings_proposals_get_all');
                    $.ajax(url)
                        .done(function (results) {
                            app.results = results;
                        })
                        .fail(function () {

                        });

                });
        }
    },
    mounted:function () {
        var url = Routing.generate('admin_trainings_proposals_get_all');
        $.ajax(url)
            .done(function (results) {
                app.results = results;
            })
            .fail(function () {

            });
    }
})