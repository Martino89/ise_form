<?php

namespace AppBundle\Services;


use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;

class UsersCmdSrv
{
    private $um;
    private $em;

    public function __construct(UserManager $um, EntityManager $em)
    {
        $this->um = $um;
        $this->em = $em;
    }

    public function deleteUser($user_id)
    {
        $user = $this->um->findUserBy(['id' => $user_id]);
        $this->um->deleteUser($user);
        return $user;
    }

    public function addUser($form_data)
    {
        $user = $this->um->createUser()
            ->setUsername($form_data["username"])
            ->setEmail($form_data["email"])
            ->setPlainPassword($form_data["password"])
            ->setEnabled(true)
            ->addRole('ROLE_ADMIN');
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    public function updateUser($form_data)
    {
        $user = $this->um->findUserBy(array("id" => $form_data["id"]));
        if ($form_data["password"] == NULL) {
            $user
                ->setUsername($form_data["username"])
                ->setEmail($form_data["email"]);
        } else {
            $user
                ->setUsername($form_data["username"])
                ->setEmail($form_data["email"])
                ->setPlainPassword($form_data["password"]);
        }
        $this->um->updateUser($user);
        return $user;
    }

}