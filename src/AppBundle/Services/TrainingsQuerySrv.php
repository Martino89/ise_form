<?php
/**
 * Created by PhpStorm.
 * User: mbiegalski
 * Date: 2017-04-06
 * Time: 16:11
 */

namespace AppBundle\Services;

use AppBundle\Repository\TrainingsQueryRep;
use DictionaryBundle\Services\DictionaryValuesQuerySrv;

class TrainingsQuerySrv
{
    private $rep;
    private $dictSrv;

    /**
     * TrainingsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(TrainingsQueryRep $rep,DictionaryValuesQuerySrv $dictSrv)
    {
        $this->rep = $rep;
        $this->dictSrv = $dictSrv;
    }

    public function getAll($filter = null)
    {
        $result = $this->rep->get($filter);

        return $result;
    }
    
    public function getVoivodship()
    {
        $voivodship = $this->dictSrv->getValuesByDictSlag('wojewodztwa');
        return $voivodship;
    }

    public function getType()
    {
        $types = $this->dictSrv->getValuesByDictSlag('rodzaje-form-doskonalenia');
        return $types;
    }
    
    public function getSubjects()
    {
        $subjects = $this->dictSrv->getValuesByDictSlag('przedmioty');
        return $subjects;
    }
}