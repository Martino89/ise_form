<?php

namespace AppBundle\Services;


use AppBundle\Repository\TrainingsProposalsQueryRep;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DictionaryBundle\Services\DictionaryValuesQuerySrv;

class TrainingsProposalsQuerySrv extends Controller
{
    private $rep;
    private $dictSrv;

    public function __construct(TrainingsProposalsQueryRep $rep,DictionaryValuesQuerySrv $dictSrv)
    {
        $this->rep = $rep;
        $this->dictSrv = $dictSrv;
    }

    public function getType()
    {
        $types = $this->dictSrv->getValuesByDictSlag('rodzaje-form-doskonalenia');
        return $types;
    }

    public function getSubjects()
    {
        $subjects = $this->dictSrv->getAllValues('przedmioty');

        $tabSubjects = [];

        foreach ($subjects as $subject)
        {
            $tabSubjects[$subject->getValue()] = $subject->getId();
        }

        ksort($tabSubjects,SORT_STRING | SORT_FLAG_CASE);
        return $tabSubjects;
    }

    public function getAll()
    {
        $proposals = $this->rep->getAllTrainingsProposals();

        foreach ($proposals as &$proposal)
        {
            $proposalId = $proposal['trp_id'];
            $subjects = $this->rep->getSubjectsByTrainingsProposalsId($proposalId);
            $proposal['subjects'] = $subjects;
        }
        return $proposals;
    }
}