<?php

namespace AppBundle\Services;


use AppBundle\Repository\CoursesProposalsCmdRep;


class CoursesProposalsCmdSrv
{
    private $rep;

    public function __construct(CoursesProposalsCmdRep $rep)
    {

        $this->rep = $rep;
    }

    /**
     * @param $form
     * @param $courseProposal
     * @param $subjects_proposals
     */
    public function saveCourseProposal($form, $courseProposal, $subjects_proposals)
    {
         $this->rep->saveCourseProposal($form, $courseProposal, $subjects_proposals);
    }

    public function deleteCourseProposalById($id)
    {
        $delete = $this->rep->deleteCourseProposalById($id);
    }
}