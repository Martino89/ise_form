<?php


namespace AppBundle\Services;

use AppBundle\Repository\TrainingsProposalsCmdRep;

class TrainingsProposalsCmdSrv
{

    const value = 'value';

    private $rep;

    public function __construct(TrainingsProposalsCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function saveTrainingsProposal($IseTrainingProposals, $form, $IseTrpsSubs)
    {
        $this->rep->saveTrainingsProposal($IseTrainingProposals, $form, $IseTrpsSubs);
    }

    public function deleteTrainingProposalById($id)
    {
        $delete = $this->rep->deleteTrainingProposalById($id);
    }
}