<?php
/**
 * Created by PhpStorm.
 * User: mbiegalski
 * Date: 2017-04-06
 * Time: 16:11
 */

namespace AppBundle\Services;

use AppBundle\Repository\CoursesQueryRep;
use DictionaryBundle\Services\DictionaryValuesQuerySrv;


class CoursesQuerySrv
{
    private $rep;
    private $srvDict;

    /**
     * TrainingsQuerySrv constructor.
     * @param $rep
     * @param $srvDict
     */
    public function __construct(CoursesQueryRep $rep, DictionaryValuesQuerySrv $srvDict)
    {
        $this->rep = $rep;
        $this->srvDict = $srvDict;
    }


    public function getCoursesWith($filter = null, $page, $limit)
    {
        if(!$page) return [];
        $offset = ($page-1) * $limit;
        $result = $this->rep->get($filter, $offset, $limit);

        foreach ($result['result'] as $key => &$elem)
        {
            if(sizeof($elem)==1)
            {
                $elem = $elem[0];
            }
            else
            {
                $subs = [];
                foreach ($elem as $item)
                {
                    $subs[] = $item['subjects'];
                }
                $elem=$elem[0];
                $elem['subjects'] = $subs;
            }

        }
        return $result;

    }


    public function getVoivodeship()
    {

        $values  = $this->srvDict->getAllValues('wojewodztwa');


        $temp=[];
        foreach ($values as $item)
        {
            $temp[$item->getValue()] = $item->getId();
        }



        ksort($temp, SORT_FLAG_CASE|SORT_STRING|SORT_NATURAL);
        return $temp;

    }



}