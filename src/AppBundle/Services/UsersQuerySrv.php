<?php

namespace AppBundle\Services;


use FOS\UserBundle\Doctrine\UserManager;

class UsersQuerySrv
{
    private $um;

    public function __construct(UserManager $um)
    {
        $this->um = $um;
    }

    public function getAllUsers()
    {
        $users = $this->um->findUsers();
        return $users;
    }

    public function getUserById($user_id)
    {
        $user = $this->um->findUserBy(['id' => $user_id]);
        return $user;
    }

}