<?php

namespace AppBundle\Services;


use AppBundle\Repository\CoursesProposalsQueryRep;
use DictionaryBundle\Services\DictionaryValuesQuerySrv;
use Symfony\Component\Config\Definition\Exception\Exception;


class CoursesProposalsQuerySrv
{
    private $rep;
    private $srvDict;

    public function __construct(CoursesProposalsQueryRep $rep, DictionaryValuesQuerySrv $srvDict)
    {
        $this->rep = $rep;
        $this->srvDict = $srvDict;
    }

    public function getType()
    {
        $values = $this->srvDict->getAllValues('sposoby-rekrutacji');
        $temp = [];
        foreach ($values as $item) {
            $temp[$item->getValue()] = $item->getId();
        }
        ksort($temp, SORT_FLAG_CASE | SORT_STRING | SORT_NATURAL);

        return $temp;
    }

    public function getDegree()
    {
        $values = $this->srvDict->getAllValues('stopnie-ksztalcenia');
        $temp = [];
        foreach ($values as $item) {
            $temp[$item->getValue()] = $item->getId();
        }
        ksort($temp, SORT_FLAG_CASE | SORT_STRING | SORT_NATURAL);

        return $temp;

    }

    public function getQualifications()
    {
        $values = $this->srvDict->getAllValues('kwalifikacje');
        $temp = [];
        foreach ($values as $item) {
            $temp[$item->getValue()] = $item->getId();
        }
        ksort($temp, SORT_FLAG_CASE | SORT_STRING | SORT_NATURAL);

        return $temp;

    }

    public function getMethods()
    {
        $values = $this->srvDict->getAllValues('rodzaje-form-ksztalcenia');
        $temp = [];
        foreach ($values as $item) {
            $temp[$item->getValue()] = $item->getId();
        }
        ksort($temp, SORT_FLAG_CASE | SORT_STRING | SORT_NATURAL);

        return $temp;


    }

    public function getSubjects()
    {

        $values = $this->srvDict->getAllValues('przedmioty');


        $temp = [];
        foreach ($values as $item) {
            $temp[$item->getValue()] = $item->getId();
        }


        ksort($temp, SORT_FLAG_CASE | SORT_STRING | SORT_NATURAL);
        return $temp;

    }

    public function getAll()
    {
        $proposals = $this->rep->getAllCoursesProposals();
        foreach ($proposals as &$proposal) {
            $proposal["subjects"] = $this->rep->getSubjectsByProposalId($proposal["copr_id"]);
        }
        return $proposals;
    }


}