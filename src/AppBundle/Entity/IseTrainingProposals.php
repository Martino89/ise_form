<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * IseTrainingProposals
 *
 * @ORM\Table(name="ise_training_proposals", indexes={@ORM\Index(name="fk_trp_trt", columns={"trt_id"})})
 * @ORM\Entity
 */
class IseTrainingProposals
{



    /**
     * @var string
     *
     * @ORM\Column(name="trp_fac_name", type="string", length=300, nullable=true)
     */
    private $trpFacName;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_fac_town", type="string", length=100, nullable=true)
     */
    private $trpFacTown;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_fac_address", type="string", length=200, nullable=true)
     */
    private $trpFacAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_aut_email", type="string", length=100, nullable=true)
     */
    private $trpAutEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_aut_name", type="string", length=200, nullable=true)
     */
    private $trpAutName;

    /**
     * @var integer
     *
     * @ORM\Column(name="trp_date", type="integer", nullable=false)
     */
    private $trpDate;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_tra_name", type="string", length=300, nullable=true)
     */
    private $trpTraName;

    /**
     * @var integer
     *
     * @ORM\Column(name="trt_id", type="integer", nullable=false)
     */
    private $trtId;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_tra_schoolyear", type="string", length=9, nullable=true)
     */
    private $trpTraSchoolyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="trp_tra_hours", type="integer", nullable=false)
     */
    private $trpTraHours;

    /**
     * @var string
     *
     * @ORM\Column(name="trp_tra_description", type="string", length=300, nullable=true)
     */
    private $trpTraDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="trp_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $trpId;

    private $subject;

    /**
     * @return ArrayCollection
     */
    public function getSubject()
    {
        return $this->subject;
    }

    public function __construct()
    {
        $this->subject = new ArrayCollection();
    }


    /**
     * Set trpFacName
     *
     * @param string $trpFacName
     *
     * @return IseTrainingProposals
     */
    public function setTrpFacName($trpFacName)
    {
        $this->trpFacName = $trpFacName;

        return $this;
    }

    /**
     * Get trpFacName
     *
     * @return string
     */
    public function getTrpFacName()
    {
        return $this->trpFacName;
    }

    /**
     * Set trpFacTown
     *
     * @param string $trpFacTown
     *
     * @return IseTrainingProposals
     */
    public function setTrpFacTown($trpFacTown)
    {
        $this->trpFacTown = $trpFacTown;

        return $this;
    }

    /**
     * Get trpFacTown
     *
     * @return string
     */
    public function getTrpFacTown()
    {
        return $this->trpFacTown;
    }

    /**
     * Set trpFacAddress
     *
     * @param string $trpFacAddress
     *
     * @return IseTrainingProposals
     */
    public function setTrpFacAddress($trpFacAddress)
    {
        $this->trpFacAddress = $trpFacAddress;

        return $this;
    }

    /**
     * Get trpFacAddress
     *
     * @return string
     */
    public function getTrpFacAddress()
    {
        return $this->trpFacAddress;
    }

    /**
     * Set trpAutEmail
     *
     * @param string $trpAutEmail
     *
     * @return IseTrainingProposals
     */
    public function setTrpAutEmail($trpAutEmail)
    {
        $this->trpAutEmail = $trpAutEmail;

        return $this;
    }

    /**
     * Get trpAutEmail
     *
     * @return string
     */
    public function getTrpAutEmail()
    {
        return $this->trpAutEmail;
    }

    /**
     * Set trpAutName
     *
     * @param string $trpAutName
     *
     * @return IseTrainingProposals
     */
    public function setTrpAutName($trpAutName)
    {
        $this->trpAutName = $trpAutName;

        return $this;
    }

    /**
     * Get trpAutName
     *
     * @return string
     */
    public function getTrpAutName()
    {
        return $this->trpAutName;
    }

    /**
     * Set trpDate
     *
     * @param integer $trpDate
     *
     * @return IseTrainingProposals
     */
    public function setTrpDate($trpDate)
    {
        $this->trpDate = $trpDate;

        return $this;
    }

    /**
     * Get trpDate
     *
     * @return integer
     */
    public function getTrpDate()
    {
        return $this->trpDate;
    }

    /**
     * Set trpTraName
     *
     * @param string $trpTraName
     *
     * @return IseTrainingProposals
     */
    public function setTrpTraName($trpTraName)
    {
        $this->trpTraName = $trpTraName;

        return $this;
    }

    /**
     * Get trpTraName
     *
     * @return string
     */
    public function getTrpTraName()
    {
        return $this->trpTraName;
    }

    /**
     * Set trtId
     *
     * @param integer $trtId
     *
     * @return IseTrainingProposals
     */
    public function setTrtId($trtId)
    {
        $this->trtId = $trtId;

        return $this;
    }

    /**
     * Get trtId
     *
     * @return integer
     */
    public function getTrtId()
    {
        return $this->trtId;
    }

    /**
     * Set trpTraSchoolyear
     *
     * @param string $trpTraSchoolyear
     *
     * @return IseTrainingProposals
     */
    public function setTrpTraSchoolyear($trpTraSchoolyear)
    {
        $this->trpTraSchoolyear = $trpTraSchoolyear;

        return $this;
    }

    /**
     * Get trpTraSchoolyear
     *
     * @return string
     */
    public function getTrpTraSchoolyear()
    {
        return $this->trpTraSchoolyear;
    }

    /**
     * Set trpTraHours
     *
     * @param integer $trpTraHours
     *
     * @return IseTrainingProposals
     */
    public function setTrpTraHours($trpTraHours)
    {
        $this->trpTraHours = $trpTraHours;

        return $this;
    }

    /**
     * Get trpTraHours
     *
     * @return integer
     */
    public function getTrpTraHours()
    {
        return $this->trpTraHours;
    }

    /**
     * Set trpTraDescription
     *
     * @param string $trpTraDescription
     *
     * @return IseTrainingProposals
     */
    public function setTrpTraDescription($trpTraDescription)
    {
        $this->trpTraDescription = $trpTraDescription;

        return $this;
    }

    /**
     * Get trpTraDescription
     *
     * @return string
     */
    public function getTrpTraDescription()
    {
        return $this->trpTraDescription;
    }

    /**
     * Get trpId
     *
     * @return integer
     */
    public function getTrpId()
    {
        return $this->trpId;
    }
}
