<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCourseTypes
 *
 * @ORM\Table(name="ise_course_types", indexes={@ORM\Index(name="fk_cot_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseCourseTypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="cot_name", type="string", length=100, nullable=true)
     */
    private $cotName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="cot_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cotId;



    /**
     * Set cotName
     *
     * @param string $cotName
     *
     * @return IseCourseTypes
     */
    public function setCotName($cotName)
    {
        $this->cotName = $cotName;

        return $this;
    }

    /**
     * Get cotName
     *
     * @return string
     */
    public function getCotName()
    {
        return $this->cotName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseCourseTypes
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseCourseTypes
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get cotId
     *
     * @return integer
     */
    public function getCotId()
    {
        return $this->cotId;
    }
}
