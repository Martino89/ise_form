<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseTrpsSubs
 *
 * @ORM\Table(name="ise_trps_subs", indexes={@ORM\Index(name="sub_id", columns={"sub_id"})})
 * @ORM\Entity
 */
class IseTrpsSubs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="trp_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $trpId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $subId;



    /**
     * Set trpId
     *
     * @param integer $trpId
     *
     * @return IseTrpsSubs
     */
    public function setTrpId($trpId)
    {
        $this->trpId = $trpId;

        return $this;
    }

    /**
     * Get trpId
     *
     * @return integer
     */
    public function getTrpId()
    {
        return $this->trpId;
    }

    /**
     * Set subId
     *
     * @param integer $subId
     *
     * @return IseTrpsSubs
     */
    public function setSubId($subId)
    {
        $this->subId = $subId;

        return $this;
    }

    /**
     * Get subId
     *
     * @return integer
     */
    public function getSubId()
    {
        return $this->subId;
    }
}
