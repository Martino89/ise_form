<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCourses
 *
 * @ORM\Table(name="ise_courses", indexes={@ORM\Index(name="fk_cou_cot", columns={"cot_id"}), @ORM\Index(name="fk_cou_col", columns={"col_id"}), @ORM\Index(name="fk_cou_coq", columns={"coq_id"}), @ORM\Index(name="fk_cou_coa", columns={"coa_id"}), @ORM\Index(name="fk_cou_fac", columns={"fac_id"}), @ORM\Index(name="fk_cou_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseCourses
{
    /**
     * @var string
     *
     * @ORM\Column(name="cou_name", type="string", length=300, nullable=true)
     */
    private $couName;

    /**
     * @var string
     *
     * @ORM\Column(name="cou_academicyear", type="string", length=9, nullable=true)
     */
    private $couAcademicyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="cou_semesters", type="integer", nullable=true)
     */
    private $couSemesters;

    /**
     * @var string
     *
     * @ORM\Column(name="cou_description", type="text", length=16777215, nullable=true)
     */
    private $couDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="cot_id", type="integer", nullable=false)
     */
    private $cotId;

    /**
     * @var integer
     *
     * @ORM\Column(name="col_id", type="integer", nullable=false)
     */
    private $colId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coq_id", type="integer", nullable=true)
     */
    private $coqId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coa_id", type="integer", nullable=true)
     */
    private $coaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fac_id", type="integer", nullable=false)
     */
    private $facId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="cou_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $couId;



    /**
     * Set couName
     *
     * @param string $couName
     *
     * @return IseCourses
     */
    public function setCouName($couName)
    {
        $this->couName = $couName;

        return $this;
    }

    /**
     * Get couName
     *
     * @return string
     */
    public function getCouName()
    {
        return $this->couName;
    }

    /**
     * Set couAcademicyear
     *
     * @param string $couAcademicyear
     *
     * @return IseCourses
     */
    public function setCouAcademicyear($couAcademicyear)
    {
        $this->couAcademicyear = $couAcademicyear;

        return $this;
    }

    /**
     * Get couAcademicyear
     *
     * @return string
     */
    public function getCouAcademicyear()
    {
        return $this->couAcademicyear;
    }

    /**
     * Set couSemesters
     *
     * @param integer $couSemesters
     *
     * @return IseCourses
     */
    public function setCouSemesters($couSemesters)
    {
        $this->couSemesters = $couSemesters;

        return $this;
    }

    /**
     * Get couSemesters
     *
     * @return integer
     */
    public function getCouSemesters()
    {
        return $this->couSemesters;
    }

    /**
     * Set couDescription
     *
     * @param string $couDescription
     *
     * @return IseCourses
     */
    public function setCouDescription($couDescription)
    {
        $this->couDescription = $couDescription;

        return $this;
    }

    /**
     * Get couDescription
     *
     * @return string
     */
    public function getCouDescription()
    {
        return $this->couDescription;
    }

    /**
     * Set cotId
     *
     * @param integer $cotId
     *
     * @return IseCourses
     */
    public function setCotId($cotId)
    {
        $this->cotId = $cotId;

        return $this;
    }

    /**
     * Get cotId
     *
     * @return integer
     */
    public function getCotId()
    {
        return $this->cotId;
    }

    /**
     * Set colId
     *
     * @param integer $colId
     *
     * @return IseCourses
     */
    public function setColId($colId)
    {
        $this->colId = $colId;

        return $this;
    }

    /**
     * Get colId
     *
     * @return integer
     */
    public function getColId()
    {
        return $this->colId;
    }

    /**
     * Set coqId
     *
     * @param integer $coqId
     *
     * @return IseCourses
     */
    public function setCoqId($coqId)
    {
        $this->coqId = $coqId;

        return $this;
    }

    /**
     * Get coqId
     *
     * @return integer
     */
    public function getCoqId()
    {
        return $this->coqId;
    }

    /**
     * Set coaId
     *
     * @param integer $coaId
     *
     * @return IseCourses
     */
    public function setCoaId($coaId)
    {
        $this->coaId = $coaId;

        return $this;
    }

    /**
     * Get coaId
     *
     * @return integer
     */
    public function getCoaId()
    {
        return $this->coaId;
    }

    /**
     * Set facId
     *
     * @param integer $facId
     *
     * @return IseCourses
     */
    public function setFacId($facId)
    {
        $this->facId = $facId;

        return $this;
    }

    /**
     * Get facId
     *
     * @return integer
     */
    public function getFacId()
    {
        return $this->facId;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseCourses
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseCourses
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get couId
     *
     * @return integer
     */
    public function getCouId()
    {
        return $this->couId;
    }
}
