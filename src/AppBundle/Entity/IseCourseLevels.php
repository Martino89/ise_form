<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCourseLevels
 *
 * @ORM\Table(name="ise_course_levels", indexes={@ORM\Index(name="fk_col_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseCourseLevels
{
    /**
     * @var string
     *
     * @ORM\Column(name="col_name", type="string", length=100, nullable=true)
     */
    private $colName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="col_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $colId;



    /**
     * Set colName
     *
     * @param string $colName
     *
     * @return IseCourseLevels
     */
    public function setColName($colName)
    {
        $this->colName = $colName;

        return $this;
    }

    /**
     * Get colName
     *
     * @return string
     */
    public function getColName()
    {
        return $this->colName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseCourseLevels
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseCourseLevels
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get colId
     *
     * @return integer
     */
    public function getColId()
    {
        return $this->colId;
    }
}
