<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCoprsSubs
 *
 * @ORM\Table(name="ise_coprs_subs", indexes={@ORM\Index(name="fk_coprs_subs_sub", columns={"sub_id"})})
 * @ORM\Entity
 */
class IseCoprsSubs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="copr_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coprId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $subId;



    /**
     * Set coprId
     *
     * @param integer $coprId
     *
     * @return IseCoprsSubs
     */
    public function setCoprId($coprId)
    {
        $this->coprId = $coprId;

        return $this;
    }

    /**
     * Get coprId
     *
     * @return integer
     */
    public function getCoprId()
    {
        return $this->coprId;
    }

    /**
     * Set subId
     *
     * @param integer $subId
     *
     * @return IseCoprsSubs
     */
    public function setSubId($subId)
    {
        $this->subId = $subId;

        return $this;
    }

    /**
     * Get subId
     *
     * @return integer
     */
    public function getSubId()
    {
        return $this->subId;
    }
}
