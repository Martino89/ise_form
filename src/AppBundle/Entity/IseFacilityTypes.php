<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseFacilityTypes
 *
 * @ORM\Table(name="ise_facility_types", indexes={@ORM\Index(name="fk_fat_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseFacilityTypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="fat_name", type="string", length=100, nullable=true)
     */
    private $fatName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="fat_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $fatId;



    /**
     * Set fatName
     *
     * @param string $fatName
     *
     * @return IseFacilityTypes
     */
    public function setFatName($fatName)
    {
        $this->fatName = $fatName;

        return $this;
    }

    /**
     * Get fatName
     *
     * @return string
     */
    public function getFatName()
    {
        return $this->fatName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseFacilityTypes
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseFacilityTypes
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get fatId
     *
     * @return integer
     */
    public function getFatId()
    {
        return $this->fatId;
    }
}
