<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * IseCourseProposals
 *
 * @ORM\Table(name="ise_course_proposals", indexes={@ORM\Index(name="fk_copr_cot", columns={"cot_id"}), @ORM\Index(name="fk_copr_col", columns={"col_id"}), @ORM\Index(name="fk_copr_coq", columns={"coq_id"}), @ORM\Index(name="fk_copr_coa", columns={"coa_id"})})
 * @ORM\Entity
 */
class IseCourseProposals
{
    private $subjects;

    /**
     * IseCourseProposals constructor.
     * @param $subjects
     */
    public function __construct()
    {
        $this->subjects = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getSubjects()
    {
        return $this->subjects;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="copr_fac_name", type="string", length=300, nullable=true)
     */
    private $coprFacName;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_fac_town", type="string", length=100, nullable=true)
     */
    private $coprFacTown;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_resp_fac_name", type="string", length=300, nullable=true)
     */
    private $coprRespFacName;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_resp_fac_town", type="string", length=100, nullable=true)
     */
    private $coprRespFacTown;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_resp_fac_address", type="string", length=200, nullable=true)
     */
    private $coprRespFacAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_resp_fac_phone", type="string", length=100, nullable=true)
     */
    private $coprRespFacPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_resp_fac_www", type="string", length=100, nullable=true)
     */
    private $coprRespFacWww;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_resp_fac_email", type="string", length=100, nullable=true)
     */
    private $coprRespFacEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_aut_email", type="string", length=100, nullable=true)
     */
    private $coprAutEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_aut_name", type="string", length=200, nullable=true)
     */
    private $coprAutName;

    /**
     * @var integer
     *
     * @ORM\Column(name="copr_date", type="integer", nullable=false)
     */
    private $coprDate;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_cou_name", type="string", length=300, nullable=true)
     */
    private $coprCouName;

    /**
     * @var integer
     *
     * @ORM\Column(name="cot_id", type="integer", nullable=false)
     */
    private $cotId;

    /**
     * @var integer
     *
     * @ORM\Column(name="col_id", type="integer", nullable=false)
     */
    private $colId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coq_id", type="integer", nullable=false)
     */
    private $coqId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coa_id", type="integer", nullable=false)
     */
    private $coaId;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_cou_academicyear", type="string", length=9, nullable=true)
     */
    private $coprCouAcademicyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="copr_cop_begin_timestamp", type="integer", nullable=true)
     */
    private $coprCopBeginTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="copr_cop_end_timestamp", type="integer", nullable=true)
     */
    private $coprCopEndTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="copr_cou_semesters", type="integer", nullable=false)
     */
    private $coprCouSemesters;

    /**
     * @var string
     *
     * @ORM\Column(name="copr_cou_description", type="string", length=300, nullable=true)
     */
    private $coprCouDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="copr_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coprId;



    /**
     * Set coprFacName
     *
     * @param string $coprFacName
     *
     * @return IseCourseProposals
     */
    public function setCoprFacName($coprFacName)
    {
        $this->coprFacName = $coprFacName;

        return $this;
    }

    /**
     * Get coprFacName
     *
     * @return string
     */
    public function getCoprFacName()
    {
        return $this->coprFacName;
    }

    /**
     * Set coprFacTown
     *
     * @param string $coprFacTown
     *
     * @return IseCourseProposals
     */
    public function setCoprFacTown($coprFacTown)
    {
        $this->coprFacTown = $coprFacTown;

        return $this;
    }

    /**
     * Get coprFacTown
     *
     * @return string
     */
    public function getCoprFacTown()
    {
        return $this->coprFacTown;
    }

    /**
     * Set coprRespFacName
     *
     * @param string $coprRespFacName
     *
     * @return IseCourseProposals
     */
    public function setCoprRespFacName($coprRespFacName)
    {
        $this->coprRespFacName = $coprRespFacName;

        return $this;
    }

    /**
     * Get coprRespFacName
     *
     * @return string
     */
    public function getCoprRespFacName()
    {
        return $this->coprRespFacName;
    }

    /**
     * Set coprRespFacTown
     *
     * @param string $coprRespFacTown
     *
     * @return IseCourseProposals
     */
    public function setCoprRespFacTown($coprRespFacTown)
    {
        $this->coprRespFacTown = $coprRespFacTown;

        return $this;
    }

    /**
     * Get coprRespFacTown
     *
     * @return string
     */
    public function getCoprRespFacTown()
    {
        return $this->coprRespFacTown;
    }

    /**
     * Set coprRespFacAddress
     *
     * @param string $coprRespFacAddress
     *
     * @return IseCourseProposals
     */
    public function setCoprRespFacAddress($coprRespFacAddress)
    {
        $this->coprRespFacAddress = $coprRespFacAddress;

        return $this;
    }

    /**
     * Get coprRespFacAddress
     *
     * @return string
     */
    public function getCoprRespFacAddress()
    {
        return $this->coprRespFacAddress;
    }

    /**
     * Set coprRespFacPhone
     *
     * @param string $coprRespFacPhone
     *
     * @return IseCourseProposals
     */
    public function setCoprRespFacPhone($coprRespFacPhone)
    {
        $this->coprRespFacPhone = $coprRespFacPhone;

        return $this;
    }

    /**
     * Get coprRespFacPhone
     *
     * @return string
     */
    public function getCoprRespFacPhone()
    {
        return $this->coprRespFacPhone;
    }

    /**
     * Set coprRespFacWww
     *
     * @param string $coprRespFacWww
     *
     * @return IseCourseProposals
     */
    public function setCoprRespFacWww($coprRespFacWww)
    {
        $this->coprRespFacWww = $coprRespFacWww;

        return $this;
    }

    /**
     * Get coprRespFacWww
     *
     * @return string
     */
    public function getCoprRespFacWww()
    {
        return $this->coprRespFacWww;
    }

    /**
     * Set coprRespFacEmail
     *
     * @param string $coprRespFacEmail
     *
     * @return IseCourseProposals
     */
    public function setCoprRespFacEmail($coprRespFacEmail)
    {
        $this->coprRespFacEmail = $coprRespFacEmail;

        return $this;
    }

    /**
     * Get coprRespFacEmail
     *
     * @return string
     */
    public function getCoprRespFacEmail()
    {
        return $this->coprRespFacEmail;
    }

    /**
     * Set coprAutEmail
     *
     * @param string $coprAutEmail
     *
     * @return IseCourseProposals
     */
    public function setCoprAutEmail($coprAutEmail)
    {
        $this->coprAutEmail = $coprAutEmail;

        return $this;
    }

    /**
     * Get coprAutEmail
     *
     * @return string
     */
    public function getCoprAutEmail()
    {
        return $this->coprAutEmail;
    }

    /**
     * Set coprAutName
     *
     * @param string $coprAutName
     *
     * @return IseCourseProposals
     */
    public function setCoprAutName($coprAutName)
    {
        $this->coprAutName = $coprAutName;

        return $this;
    }

    /**
     * Get coprAutName
     *
     * @return string
     */
    public function getCoprAutName()
    {
        return $this->coprAutName;
    }

    /**
     * Set coprDate
     *
     * @param integer $coprDate
     *
     * @return IseCourseProposals
     */
    public function setCoprDate($coprDate)
    {
        $this->coprDate = $coprDate;

        return $this;
    }

    /**
     * Get coprDate
     *
     * @return integer
     */
    public function getCoprDate()
    {
        return $this->coprDate;
    }

    /**
     * Set coprCouName
     *
     * @param string $coprCouName
     *
     * @return IseCourseProposals
     */
    public function setCoprCouName($coprCouName)
    {
        $this->coprCouName = $coprCouName;

        return $this;
    }

    /**
     * Get coprCouName
     *
     * @return string
     */
    public function getCoprCouName()
    {
        return $this->coprCouName;
    }

    /**
     * Set cotId
     *
     * @param integer $cotId
     *
     * @return IseCourseProposals
     */
    public function setCotId($cotId)
    {
        $this->cotId = $cotId;

        return $this;
    }

    /**
     * Get cotId
     *
     * @return integer
     */
    public function getCotId()
    {
        return $this->cotId;
    }

    /**
     * Set colId
     *
     * @param integer $colId
     *
     * @return IseCourseProposals
     */
    public function setColId($colId)
    {
        $this->colId = $colId;

        return $this;
    }

    /**
     * Get colId
     *
     * @return integer
     */
    public function getColId()
    {
        return $this->colId;
    }

    /**
     * Set coqId
     *
     * @param integer $coqId
     *
     * @return IseCourseProposals
     */
    public function setCoqId($coqId)
    {
        $this->coqId = $coqId;

        return $this;
    }

    /**
     * Get coqId
     *
     * @return integer
     */
    public function getCoqId()
    {
        return $this->coqId;
    }

    /**
     * Set coaId
     *
     * @param integer $coaId
     *
     * @return IseCourseProposals
     */
    public function setCoaId($coaId)
    {
        $this->coaId = $coaId;

        return $this;
    }

    /**
     * Get coaId
     *
     * @return integer
     */
    public function getCoaId()
    {
        return $this->coaId;
    }

    /**
     * Set coprCouAcademicyear
     *
     * @param string $coprCouAcademicyear
     *
     * @return IseCourseProposals
     */
    public function setCoprCouAcademicyear($coprCouAcademicyear)
    {
        $this->coprCouAcademicyear = $coprCouAcademicyear;

        return $this;
    }

    /**
     * Get coprCouAcademicyear
     *
     * @return string
     */
    public function getCoprCouAcademicyear()
    {
        return $this->coprCouAcademicyear;
    }

    /**
     * Set coprCopBeginTimestamp
     *
     * @param integer $coprCopBeginTimestamp
     *
     * @return IseCourseProposals
     */
    public function setCoprCopBeginTimestamp($coprCopBeginTimestamp)
    {
        $this->coprCopBeginTimestamp = $coprCopBeginTimestamp;

        return $this;
    }

    /**
     * Get coprCopBeginTimestamp
     *
     * @return integer
     */
    public function getCoprCopBeginTimestamp()
    {
        return $this->coprCopBeginTimestamp;
    }

    /**
     * Set coprCopEndTimestamp
     *
     * @param integer $coprCopEndTimestamp
     *
     * @return IseCourseProposals
     */
    public function setCoprCopEndTimestamp($coprCopEndTimestamp)
    {
        $this->coprCopEndTimestamp = $coprCopEndTimestamp;

        return $this;
    }

    /**
     * Get coprCopEndTimestamp
     *
     * @return integer
     */
    public function getCoprCopEndTimestamp()
    {
        return $this->coprCopEndTimestamp;
    }

    /**
     * Set coprCouSemesters
     *
     * @param integer $coprCouSemesters
     *
     * @return IseCourseProposals
     */
    public function setCoprCouSemesters($coprCouSemesters)
    {
        $this->coprCouSemesters = $coprCouSemesters;

        return $this;
    }

    /**
     * Get coprCouSemesters
     *
     * @return integer
     */
    public function getCoprCouSemesters()
    {
        return $this->coprCouSemesters;
    }

    /**
     * Set coprCouDescription
     *
     * @param string $coprCouDescription
     *
     * @return IseCourseProposals
     */
    public function setCoprCouDescription($coprCouDescription)
    {
        $this->coprCouDescription = $coprCouDescription;

        return $this;
    }

    /**
     * Get coprCouDescription
     *
     * @return string
     */
    public function getCoprCouDescription()
    {
        return $this->coprCouDescription;
    }

    /**
     * Get coprId
     *
     * @return integer
     */
    public function getCoprId()
    {
        return $this->coprId;
    }
}
