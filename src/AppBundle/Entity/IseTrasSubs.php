<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseTrasSubs
 *
 * @ORM\Table(name="ise_tras_subs", indexes={@ORM\Index(name="fk_i5lc1_ise_subs_sub", columns={"sub_id"})})
 * @ORM\Entity
 */
class IseTrasSubs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tra_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $traId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $subId;



    /**
     * Set traId
     *
     * @param integer $traId
     *
     * @return IseTrasSubs
     */
    public function setTraId($traId)
    {
        $this->traId = $traId;

        return $this;
    }

    /**
     * Get traId
     *
     * @return integer
     */
    public function getTraId()
    {
        return $this->traId;
    }

    /**
     * Set subId
     *
     * @param integer $subId
     *
     * @return IseTrasSubs
     */
    public function setSubId($subId)
    {
        $this->subId = $subId;

        return $this;
    }

    /**
     * Get subId
     *
     * @return integer
     */
    public function getSubId()
    {
        return $this->subId;
    }
}
