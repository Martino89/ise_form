<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IsePermissions
 *
 * @ORM\Table(name="ise_permissions")
 * @ORM\Entity
 */
class IsePermissions
{
    /**
     * @var string
     *
     * @ORM\Column(name="per_name", type="string", length=100, nullable=true)
     */
    private $perName;

    /**
     * @var string
     *
     * @ORM\Column(name="per_short", type="string", length=30, nullable=true)
     */
    private $perShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $perId;



    /**
     * Set perName
     *
     * @param string $perName
     *
     * @return IsePermissions
     */
    public function setPerName($perName)
    {
        $this->perName = $perName;

        return $this;
    }

    /**
     * Get perName
     *
     * @return string
     */
    public function getPerName()
    {
        return $this->perName;
    }

    /**
     * Set perShort
     *
     * @param string $perShort
     *
     * @return IsePermissions
     */
    public function setPerShort($perShort)
    {
        $this->perShort = $perShort;

        return $this;
    }

    /**
     * Get perShort
     *
     * @return string
     */
    public function getPerShort()
    {
        return $this->perShort;
    }

    /**
     * Get perId
     *
     * @return integer
     */
    public function getPerId()
    {
        return $this->perId;
    }
}
