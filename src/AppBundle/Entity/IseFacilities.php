<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseFacilities
 *
 * @ORM\Table(name="ise_facilities", indexes={@ORM\Index(name="fk_fac_parent_fac", columns={"parent_fac_id"}), @ORM\Index(name="fk_fac_use", columns={"mod_use_id"}), @ORM\Index(name="fk_fac_pro", columns={"pro_id"}), @ORM\Index(name="fk_fac_fat", columns={"fat_id"})})
 * @ORM\Entity
 */
class IseFacilities
{
    /**
     * @var string
     *
     * @ORM\Column(name="fac_name", type="string", length=300, nullable=true)
     */
    private $facName;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_named_after", type="string", length=300, nullable=true)
     */
    private $facNamedAfter;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_town", type="string", length=100, nullable=true)
     */
    private $facTown;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_postcode", type="string", length=6, nullable=true)
     */
    private $facPostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_address", type="string", length=200, nullable=true)
     */
    private $facAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_phone", type="string", length=100, nullable=true)
     */
    private $facPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_fax", type="string", length=50, nullable=true)
     */
    private $facFax;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_email", type="string", length=100, nullable=true)
     */
    private $facEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_www", type="string", length=100, nullable=true)
     */
    private $facWww;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_manager", type="string", length=100, nullable=true)
     */
    private $facManager;

    /**
     * @var string
     *
     * @ORM\Column(name="fac_description", type="text", length=16777215, nullable=true)
     */
    private $facDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="pro_id", type="integer", nullable=false)
     */
    private $proId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fat_id", type="integer", nullable=false)
     */
    private $fatId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_fac_id", type="integer", nullable=true)
     */
    private $parentFacId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="fac_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $facId;



    /**
     * Set facName
     *
     * @param string $facName
     *
     * @return IseFacilities
     */
    public function setFacName($facName)
    {
        $this->facName = $facName;

        return $this;
    }

    /**
     * Get facName
     *
     * @return string
     */
    public function getFacName()
    {
        return $this->facName;
    }

    /**
     * Set facNamedAfter
     *
     * @param string $facNamedAfter
     *
     * @return IseFacilities
     */
    public function setFacNamedAfter($facNamedAfter)
    {
        $this->facNamedAfter = $facNamedAfter;

        return $this;
    }

    /**
     * Get facNamedAfter
     *
     * @return string
     */
    public function getFacNamedAfter()
    {
        return $this->facNamedAfter;
    }

    /**
     * Set facTown
     *
     * @param string $facTown
     *
     * @return IseFacilities
     */
    public function setFacTown($facTown)
    {
        $this->facTown = $facTown;

        return $this;
    }

    /**
     * Get facTown
     *
     * @return string
     */
    public function getFacTown()
    {
        return $this->facTown;
    }

    /**
     * Set facPostcode
     *
     * @param string $facPostcode
     *
     * @return IseFacilities
     */
    public function setFacPostcode($facPostcode)
    {
        $this->facPostcode = $facPostcode;

        return $this;
    }

    /**
     * Get facPostcode
     *
     * @return string
     */
    public function getFacPostcode()
    {
        return $this->facPostcode;
    }

    /**
     * Set facAddress
     *
     * @param string $facAddress
     *
     * @return IseFacilities
     */
    public function setFacAddress($facAddress)
    {
        $this->facAddress = $facAddress;

        return $this;
    }

    /**
     * Get facAddress
     *
     * @return string
     */
    public function getFacAddress()
    {
        return $this->facAddress;
    }

    /**
     * Set facPhone
     *
     * @param string $facPhone
     *
     * @return IseFacilities
     */
    public function setFacPhone($facPhone)
    {
        $this->facPhone = $facPhone;

        return $this;
    }

    /**
     * Get facPhone
     *
     * @return string
     */
    public function getFacPhone()
    {
        return $this->facPhone;
    }

    /**
     * Set facFax
     *
     * @param string $facFax
     *
     * @return IseFacilities
     */
    public function setFacFax($facFax)
    {
        $this->facFax = $facFax;

        return $this;
    }

    /**
     * Get facFax
     *
     * @return string
     */
    public function getFacFax()
    {
        return $this->facFax;
    }

    /**
     * Set facEmail
     *
     * @param string $facEmail
     *
     * @return IseFacilities
     */
    public function setFacEmail($facEmail)
    {
        $this->facEmail = $facEmail;

        return $this;
    }

    /**
     * Get facEmail
     *
     * @return string
     */
    public function getFacEmail()
    {
        return $this->facEmail;
    }

    /**
     * Set facWww
     *
     * @param string $facWww
     *
     * @return IseFacilities
     */
    public function setFacWww($facWww)
    {
        $this->facWww = $facWww;

        return $this;
    }

    /**
     * Get facWww
     *
     * @return string
     */
    public function getFacWww()
    {
        return $this->facWww;
    }

    /**
     * Set facManager
     *
     * @param string $facManager
     *
     * @return IseFacilities
     */
    public function setFacManager($facManager)
    {
        $this->facManager = $facManager;

        return $this;
    }

    /**
     * Get facManager
     *
     * @return string
     */
    public function getFacManager()
    {
        return $this->facManager;
    }

    /**
     * Set facDescription
     *
     * @param string $facDescription
     *
     * @return IseFacilities
     */
    public function setFacDescription($facDescription)
    {
        $this->facDescription = $facDescription;

        return $this;
    }

    /**
     * Get facDescription
     *
     * @return string
     */
    public function getFacDescription()
    {
        return $this->facDescription;
    }

    /**
     * Set proId
     *
     * @param integer $proId
     *
     * @return IseFacilities
     */
    public function setProId($proId)
    {
        $this->proId = $proId;

        return $this;
    }

    /**
     * Get proId
     *
     * @return integer
     */
    public function getProId()
    {
        return $this->proId;
    }

    /**
     * Set fatId
     *
     * @param integer $fatId
     *
     * @return IseFacilities
     */
    public function setFatId($fatId)
    {
        $this->fatId = $fatId;

        return $this;
    }

    /**
     * Get fatId
     *
     * @return integer
     */
    public function getFatId()
    {
        return $this->fatId;
    }

    /**
     * Set parentFacId
     *
     * @param integer $parentFacId
     *
     * @return IseFacilities
     */
    public function setParentFacId($parentFacId)
    {
        $this->parentFacId = $parentFacId;

        return $this;
    }

    /**
     * Get parentFacId
     *
     * @return integer
     */
    public function getParentFacId()
    {
        return $this->parentFacId;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseFacilities
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseFacilities
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get facId
     *
     * @return integer
     */
    public function getFacId()
    {
        return $this->facId;
    }
}
