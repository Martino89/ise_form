<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCourseApplicationTypes
 *
 * @ORM\Table(name="ise_course_application_types", indexes={@ORM\Index(name="fk_coa_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseCourseApplicationTypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="coa_name", type="string", length=100, nullable=true)
     */
    private $coaName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="coa_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coaId;



    /**
     * Set coaName
     *
     * @param string $coaName
     *
     * @return IseCourseApplicationTypes
     */
    public function setCoaName($coaName)
    {
        $this->coaName = $coaName;

        return $this;
    }

    /**
     * Get coaName
     *
     * @return string
     */
    public function getCoaName()
    {
        return $this->coaName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseCourseApplicationTypes
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseCourseApplicationTypes
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get coaId
     *
     * @return integer
     */
    public function getCoaId()
    {
        return $this->coaId;
    }
}
