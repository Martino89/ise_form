<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCourseQualifications
 *
 * @ORM\Table(name="ise_course_qualifications", indexes={@ORM\Index(name="fk_coq_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseCourseQualifications
{
    /**
     * @var string
     *
     * @ORM\Column(name="coq_name", type="string", length=100, nullable=true)
     */
    private $coqName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="coq_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coqId;



    /**
     * Set coqName
     *
     * @param string $coqName
     *
     * @return IseCourseQualifications
     */
    public function setCoqName($coqName)
    {
        $this->coqName = $coqName;

        return $this;
    }

    /**
     * Get coqName
     *
     * @return string
     */
    public function getCoqName()
    {
        return $this->coqName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseCourseQualifications
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseCourseQualifications
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get coqId
     *
     * @return integer
     */
    public function getCoqId()
    {
        return $this->coqId;
    }
}
