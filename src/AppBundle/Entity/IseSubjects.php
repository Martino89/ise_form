<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseSubjects
 *
 * @ORM\Table(name="ise_subjects", indexes={@ORM\Index(name="fk_sub_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseSubjects
{
    /**
     * @var string
     *
     * @ORM\Column(name="sub_name", type="string", length=100, nullable=true)
     */
    private $subName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $subId;



    /**
     * Set subName
     *
     * @param string $subName
     *
     * @return IseSubjects
     */
    public function setSubName($subName)
    {
        $this->subName = $subName;

        return $this;
    }

    /**
     * Get subName
     *
     * @return string
     */
    public function getSubName()
    {
        return $this->subName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseSubjects
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseSubjects
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get subId
     *
     * @return integer
     */
    public function getSubId()
    {
        return $this->subId;
    }
}
