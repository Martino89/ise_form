<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseTrainingTypes
 *
 * @ORM\Table(name="ise_training_types", indexes={@ORM\Index(name="fk_trt_use", columns={"mod_use_id"})})
 * @ORM\Entity
 */
class IseTrainingTypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="trt_name", type="string", length=100, nullable=true)
     */
    private $trtName;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="trt_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $trtId;



    /**
     * Set trtName
     *
     * @param string $trtName
     *
     * @return IseTrainingTypes
     */
    public function setTrtName($trtName)
    {
        $this->trtName = $trtName;

        return $this;
    }

    /**
     * Get trtName
     *
     * @return string
     */
    public function getTrtName()
    {
        return $this->trtName;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseTrainingTypes
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseTrainingTypes
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get trtId
     *
     * @return integer
     */
    public function getTrtId()
    {
        return $this->trtId;
    }
}
