<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseUsePer
 *
 * @ORM\Table(name="ise_use_per", indexes={@ORM\Index(name="fk_pers_per", columns={"per_id"}), @ORM\Index(name="fk_uses_use", columns={"use_id"})})
 * @ORM\Entity
 */
class IseUsePer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="use_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $useId;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $perId;



    /**
     * Set useId
     *
     * @param integer $useId
     *
     * @return IseUsePer
     */
    public function setUseId($useId)
    {
        $this->useId = $useId;

        return $this;
    }

    /**
     * Get useId
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->useId;
    }

    /**
     * Set perId
     *
     * @param integer $perId
     *
     * @return IseUsePer
     */
    public function setPerId($perId)
    {
        $this->perId = $perId;

        return $this;
    }

    /**
     * Get perId
     *
     * @return integer
     */
    public function getPerId()
    {
        return $this->perId;
    }
}
