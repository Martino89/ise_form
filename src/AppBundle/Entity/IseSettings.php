<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseSettings
 *
 * @ORM\Table(name="ise_settings")
 * @ORM\Entity
 */
class IseSettings
{
    /**
     * @var string
     *
     * @ORM\Column(name="set_label", type="string", length=200, nullable=true)
     */
    private $setLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="set_value", type="string", length=200, nullable=true)
     */
    private $setValue;

    /**
     * @var binary
     *
     * @ORM\Column(name="set_name", type="binary")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $setName;



    /**
     * Set setLabel
     *
     * @param string $setLabel
     *
     * @return IseSettings
     */
    public function setSetLabel($setLabel)
    {
        $this->setLabel = $setLabel;

        return $this;
    }

    /**
     * Get setLabel
     *
     * @return string
     */
    public function getSetLabel()
    {
        return $this->setLabel;
    }

    /**
     * Set setValue
     *
     * @param string $setValue
     *
     * @return IseSettings
     */
    public function setSetValue($setValue)
    {
        $this->setValue = $setValue;

        return $this;
    }

    /**
     * Get setValue
     *
     * @return string
     */
    public function getSetValue()
    {
        return $this->setValue;
    }

    /**
     * Get setName
     *
     * @return binary
     */
    public function getSetName()
    {
        return $this->setName;
    }
}
