<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCousSubs
 *
 * @ORM\Table(name="ise_cous_subs", indexes={@ORM\Index(name="fk_cous_subs_sub", columns={"sub_id"})})
 * @ORM\Entity
 */
class IseCousSubs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cou_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $couId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $subId;



    /**
     * Set couId
     *
     * @param integer $couId
     *
     * @return IseCousSubs
     */
    public function setCouId($couId)
    {
        $this->couId = $couId;

        return $this;
    }

    /**
     * Get couId
     *
     * @return integer
     */
    public function getCouId()
    {
        return $this->couId;
    }

    /**
     * Set subId
     *
     * @param integer $subId
     *
     * @return IseCousSubs
     */
    public function setSubId($subId)
    {
        $this->subId = $subId;

        return $this;
    }

    /**
     * Get subId
     *
     * @return integer
     */
    public function getSubId()
    {
        return $this->subId;
    }
}
