<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseTrainings
 *
 * @ORM\Table(name="ise_trainings", indexes={@ORM\Index(name="fk_tra_use", columns={"mod_use_id"}), @ORM\Index(name="fk_tra_fac", columns={"fac_id"}), @ORM\Index(name="fk_tra_trt", columns={"trt_id"})})
 * @ORM\Entity
 */
class IseTrainings
{
    /**
     * @var string
     *
     * @ORM\Column(name="tra_schoolyear", type="string", length=9, nullable=true)
     */
    private $traSchoolyear;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_name", type="string", length=300, nullable=true)
     */
    private $traName;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_hours", type="integer", nullable=true)
     */
    private $traHours;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_description", type="text", length=16777215, nullable=true)
     */
    private $traDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="trt_id", type="integer", nullable=true)
     */
    private $trtId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fac_id", type="integer", nullable=false)
     */
    private $facId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_use_id", type="integer", nullable=true)
     */
    private $modUseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mod_timestamp", type="integer", nullable=true)
     */
    private $modTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $traId;



    /**
     * Set traSchoolyear
     *
     * @param string $traSchoolyear
     *
     * @return IseTrainings
     */
    public function setTraSchoolyear($traSchoolyear)
    {
        $this->traSchoolyear = $traSchoolyear;

        return $this;
    }

    /**
     * Get traSchoolyear
     *
     * @return string
     */
    public function getTraSchoolyear()
    {
        return $this->traSchoolyear;
    }

    /**
     * Set traName
     *
     * @param string $traName
     *
     * @return IseTrainings
     */
    public function setTraName($traName)
    {
        $this->traName = $traName;

        return $this;
    }

    /**
     * Get traName
     *
     * @return string
     */
    public function getTraName()
    {
        return $this->traName;
    }

    /**
     * Set traHours
     *
     * @param integer $traHours
     *
     * @return IseTrainings
     */
    public function setTraHours($traHours)
    {
        $this->traHours = $traHours;

        return $this;
    }

    /**
     * Get traHours
     *
     * @return integer
     */
    public function getTraHours()
    {
        return $this->traHours;
    }

    /**
     * Set traDescription
     *
     * @param string $traDescription
     *
     * @return IseTrainings
     */
    public function setTraDescription($traDescription)
    {
        $this->traDescription = $traDescription;

        return $this;
    }

    /**
     * Get traDescription
     *
     * @return string
     */
    public function getTraDescription()
    {
        return $this->traDescription;
    }

    /**
     * Set trtId
     *
     * @param integer $trtId
     *
     * @return IseTrainings
     */
    public function setTrtId($trtId)
    {
        $this->trtId = $trtId;

        return $this;
    }

    /**
     * Get trtId
     *
     * @return integer
     */
    public function getTrtId()
    {
        return $this->trtId;
    }

    /**
     * Set facId
     *
     * @param integer $facId
     *
     * @return IseTrainings
     */
    public function setFacId($facId)
    {
        $this->facId = $facId;

        return $this;
    }

    /**
     * Get facId
     *
     * @return integer
     */
    public function getFacId()
    {
        return $this->facId;
    }

    /**
     * Set modUseId
     *
     * @param integer $modUseId
     *
     * @return IseTrainings
     */
    public function setModUseId($modUseId)
    {
        $this->modUseId = $modUseId;

        return $this;
    }

    /**
     * Get modUseId
     *
     * @return integer
     */
    public function getModUseId()
    {
        return $this->modUseId;
    }

    /**
     * Set modTimestamp
     *
     * @param integer $modTimestamp
     *
     * @return IseTrainings
     */
    public function setModTimestamp($modTimestamp)
    {
        $this->modTimestamp = $modTimestamp;

        return $this;
    }

    /**
     * Get modTimestamp
     *
     * @return integer
     */
    public function getModTimestamp()
    {
        return $this->modTimestamp;
    }

    /**
     * Get traId
     *
     * @return integer
     */
    public function getTraId()
    {
        return $this->traId;
    }
}
