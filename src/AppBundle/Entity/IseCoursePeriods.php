<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseCoursePeriods
 *
 * @ORM\Table(name="ise_course_periods", indexes={@ORM\Index(name="fk_course_periods", columns={"cou_id"})})
 * @ORM\Entity
 */
class IseCoursePeriods
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cou_id", type="integer", nullable=false)
     */
    private $couId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cop_begin_timestamp", type="integer", nullable=false)
     */
    private $copBeginTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="cop_end_timestamp", type="integer", nullable=false)
     */
    private $copEndTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set couId
     *
     * @param integer $couId
     *
     * @return IseCoursePeriods
     */
    public function setCouId($couId)
    {
        $this->couId = $couId;

        return $this;
    }

    /**
     * Get couId
     *
     * @return integer
     */
    public function getCouId()
    {
        return $this->couId;
    }

    /**
     * Set copBeginTimestamp
     *
     * @param integer $copBeginTimestamp
     *
     * @return IseCoursePeriods
     */
    public function setCopBeginTimestamp($copBeginTimestamp)
    {
        $this->copBeginTimestamp = $copBeginTimestamp;

        return $this;
    }

    /**
     * Get copBeginTimestamp
     *
     * @return integer
     */
    public function getCopBeginTimestamp()
    {
        return $this->copBeginTimestamp;
    }

    /**
     * Set copEndTimestamp
     *
     * @param integer $copEndTimestamp
     *
     * @return IseCoursePeriods
     */
    public function setCopEndTimestamp($copEndTimestamp)
    {
        $this->copEndTimestamp = $copEndTimestamp;

        return $this;
    }

    /**
     * Get copEndTimestamp
     *
     * @return integer
     */
    public function getCopEndTimestamp()
    {
        return $this->copEndTimestamp;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
