<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IseProvinces
 *
 * @ORM\Table(name="ise_provinces")
 * @ORM\Entity
 */
class IseProvinces
{
    /**
     * @var string
     *
     * @ORM\Column(name="pro_name", type="string", length=100, nullable=true)
     */
    private $proName;

    /**
     * @var integer
     *
     * @ORM\Column(name="pro_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $proId;



    /**
     * Set proName
     *
     * @param string $proName
     *
     * @return IseProvinces
     */
    public function setProName($proName)
    {
        $this->proName = $proName;

        return $this;
    }

    /**
     * Get proName
     *
     * @return string
     */
    public function getProName()
    {
        return $this->proName;
    }

    /**
     * Get proId
     *
     * @return integer
     */
    public function getProId()
    {
        return $this->proId;
    }
}
