<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/facilities")
 */
class AdminFacilitiesController extends Controller
{
    /**
     * @Route("/", name="admin_facilities_index")
     */
    public function indexAction()
    {
    }

    /**
     * @Route("/create", name="admin_facilities_create")
     */
    public function createAction(Request $request)
    {
    }

    /**
     * @Route("/read", name="admin_facilities_read")
     */
    public function readAction()
    {
    }

    /**
     * @Route("/update", name="admin_facilities_update")
     */
    public function updateAction(Request $request)
    {
    }

    /**
     * @Route("/delete", name="admin_facilities_delete")
     */
    public function deleteAction(Request $request)
    {
    }

    /**
     * @Route("/{id}/edit", name="admin_facilities_edit")
     */
    public function editAction($id)
    {
    }
}
