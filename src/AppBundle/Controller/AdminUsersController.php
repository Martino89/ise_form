<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * @Route("/admin/users")
 */
class AdminUsersController extends Controller
{
    /**
     * @Route("/",
     *     options = { "expose" = true },
     *     name = "admin_users_index"
     * )
     */
    public function indexAction()
    {
        return $this->render('users/index.html.twig');
    }

    /**
     * @Route("/get-all",
     *     options = { "expose" = true },
     *     name = "admin_users_get_all"
     * )
     */

    public function getAllAction()
    {
        $users_list = $this->get('AppBundle.users_query_srv')->getAllUsers();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->serialize($users_list, 'json');
        $response = new Response($result);
        return $response;
    }

    /**
     * @Route("/add",
     *     options = { "expose" = true },
     *     name = "admin_user_add"
     * )
     */

    public function addAction(Request $request)
    {
        $form_data = $request->request->all();
        $result = $this->get('AppBundle.users_cmd_srv')->addUser($form_data);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/{user_id}/delete",
     *     options = { "expose" = true },
     *     name = "admin_user_delete"
     * )
     */

    public function deleteAction($user_id)
    {
        $result = $this->get('AppBundle.users_cmd_srv')->deleteUser($user_id);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/update",
     *     options = { "expose" = true },
     *     name = "admin_user_update"
     * )
     */

    public function updateAction(Request $request)
    {
        $form_data = $request->request->all();
        $result = $this->get('AppBundle.users_cmd_srv')->updateUser($form_data);
        $response = new JsonResponse($result);
        return $response;
    }

    /**
     * @Route("/{user_id}/get",
     *     options = { "expose" = true },
     *     name = "admin_user_get"
     * )
     */

    public function getAction($user_id)
    {
        $user = $this->get('AppBundle.users_query_srv')->getUserById($user_id);
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->serialize($user, 'json');
        $response = new Response($result);
        return $response;
    }
}
