<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PHPExcel_IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PHPExcel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/generate")
 */
class ExcelGeneratorController extends Controller
{
    /**
     * @Route("/courses-proposals", options = {"expose"=true}, name="generate_courses_proposals")
     */
    public function generateCoursesProposalsAction()
    {
        $result = [1 => ['name' => 'Joe'], 2 => ['name' => 'Rick'], 3 => ['name' => 'Mike']];
        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $xls->getActiveSheet()->fromArray($result, null, 'A1');
        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
        ob_start();
        $writer->save('php://output');
        $response = new Response(ob_get_clean(), 200, ['Content-Type' => 'application/vnd.ms-excel', 'Content-Disposition' => 'attachment: filename="doc.xls']);
        return $response;


        //stream odnosił się do Bundla liuggio/phpExcel, ostatnie rozwiązanie na Stacku było prawidłowe
    }
}
