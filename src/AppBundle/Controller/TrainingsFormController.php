<?php
/**
 * Created by PhpStorm.
 * User: mjendrasik
 * Date: 2017-04-06
 * Time: 15:31
 */

namespace AppBundle\Controller;

use AppBundle\Entity\IseSubjects;
use AppBundle\Entity\IseTrainingProposals;
use AppBundle\Entity\IseTrpsSubs;
use AppBundle\Form\IseTrainingProposalsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TrainingsFormController extends Controller
{

    /**
     * @Route("/trainings/form/", name="trainings_form")
     */
    public function indexAction(Request $request)
    {

        $types = $this->get('AppBundle.trainings_proposals_query_srv')->getType();
        $subjects = $this->get('AppBundle.trainings_proposals_query_srv')->getSubjects();

        $IseTrainingProposals = new IseTrainingProposals;

        $IseTrpsSubs = new IseTrpsSubs;
        $IseSubjects = new IseSubjects;
        $IseTrainingProposals->getSubject()->add($IseSubjects);

        $form = $this->createForm(IseTrainingProposalsType::class ,$IseTrainingProposals,['subjects' => $subjects,'types' => $types]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $this->get('AppBundle.trainings_proposals_cmd_srv')->saveTrainingsProposal($IseTrainingProposals, $form, $IseTrpsSubs);
        }

        return $this->render('trainings/trainingsForm.html.twig',array(
            'form' => $form->createView()
        ));
    }

}