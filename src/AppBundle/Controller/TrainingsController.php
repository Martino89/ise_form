<?php
/**
 * Created by PhpStorm.
 * User: mbiegalski
 * Date: 2017-04-06
 * Time: 15:31
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/trainings")
 */
class TrainingsController extends Controller
{

    /**
     * @Route("/", name="trainings" , options={"expose"=true})
     */
    public function indexAction(Request $request)
    {
        return $this->render('trainings/trainings.html.twig');
    }

    /**
     * @Route("/get", name="trainings_get" , options={"expose"=true})
     */
    public function getAllAction()
    {
        $trainingsQuerySrv = $this->get('AppBundle.trainings_query_srv');
        $voivodship = $trainingsQuerySrv->getVoivodship();
        $types = $trainingsQuerySrv->getType();
        $subjects = $trainingsQuerySrv->getSubjects();
        
        $data = array(
            'voivodship' => $voivodship,
            'types' => $types,
            'subjects' => $subjects
        );
        
        return new JsonResponse($data);
    }

//    /**
//     * @Route("/get", name="trainings_post_filter")
//     */
//    public function filterAction(Request $request)
//    {
//
//    }
}