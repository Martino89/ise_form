<?php
/**
 * Created by PhpStorm.
 * User: mbiegalski
 * Date: 2017-04-06
 * Time: 15:31
 */

namespace AppBundle\Controller;

use AppBundle\Entity\IseSubjects;
use AppBundle\Form\CoursesSearchForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/courses")
 */
class CoursesController extends Controller
{

    /**
     * @Route("/", name="courses")
     */
    public function indexAction(Request $request)
    {
        $courses_data = $this->get('appbundle.courses_query_srv')->getCoursesWith(null, 1, 10);
        dump($courses_data);
        return $this->render('courses/courseSearch.html.twig');
    }
    /**
     * @Route("/data/{page}/{limit}", name="get_courses_data", options = {"expose"=true})
     */
    public function getSelectDataAction($page, $limit, Request $request)
    {

        $fil = $request->request->all();

        $voivoceship = $this->get('appbundle.courses_query_srv')->getVoivodeship();
        $subjects_list = $this->get('appbundle.courses_proposals_query_srv')->getSubjects();
        $qualifications  = $this->get('appbundle.courses_proposals_query_srv')->getQualifications();
        $degree  = $this->get('appbundle.courses_proposals_query_srv')->getDegree();
        $courses = $this->get('appbundle.courses_query_srv')->getCoursesWith(empty($fil) ? null : $fil, $page, $limit);
        $courses_data = $courses['result'];
        $courses_num = $courses['quantity'];
        $courses_pages = $courses['pages'];

        return new JsonResponse([
            'voivodeship' => $voivoceship,
            'subjects' => $subjects_list,
            'qualifications' => $qualifications,
            'degree' => $degree,
            'courses_data' => $courses_data,
            'courses_quantity' => $courses_num,
            'courses_pages' => $courses_pages,
        ]);
    }

    /**
     * @Route("/read", name="courses_read")
     */
    public function readAction()
    {

    }

    /**
     * @Route("/filter/", name="courses_filter", options = {"expose"=true})
     */
    public function filterAction( Request $request )
    {
        $filter_data = $request->request->all();

        $courses = $this->get('appbundle.courses_query_srv')->getCoursesWith($filter_data, 1, 10);
        $courses_data = $courses['result'];
        $courses_num = $courses['quantity'];
        $courses_pages = $courses['pages'];
        return new JsonResponse([
            'courses_data' => $courses_data,
            'courses_quantity' => $courses_num,
            'courses_pages' => $courses_pages,
        ]);


    }
}