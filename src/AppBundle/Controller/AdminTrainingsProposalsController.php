<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/trainings/proposals")
 */
class AdminTrainingsProposalsController extends Controller
{
    /**
     * @Route("/", name="admin_trainings_proposals_index")
     */
    public function indexAction()
    {
        $result = $this->get('AppBundle.trainings_proposals_query_srv')->getAll();
        //dump($result);
        return $this->render('admin/trainingsProposals.html.twig',array(
        ));
    }

    /**
     * @Route("/{$id}/get", name="admin_trainings_proposals_get")
     */
    public function getAction($id)
    {
    }
//viewAction

    /**
     * @Route("/get/all", name="admin_trainings_proposals_get_all", options = { "expose" = true })
     */
    public function getAllAction()
    {
        $result = $this->get('AppBundle.trainings_proposals_query_srv')->getAll();

        return new JsonResponse($result);
    }
//readAction

    /**
     * @Route("/delete", name="admin_trainings_proposals_delete", options = { "expose" = true })
     */
    public function deleteAction(Request $request)
    {
        $id = $request->request->get('element');

        $delete = $this->get('AppBundle.trainings_proposals_cmd_srv')->deleteTrainingProposalById($id);
        return new Response('usuniete');
    }

}
