<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/dictionary")
 */
class AdminDictionaryController extends Controller
{
    /**
     * @Route("/", options = { "expose" = true }, name="admin_dictionary_index")
     */
    public function indexAction()
    {
        return $this->render('dictionary/index.html.twig');
    }
}
