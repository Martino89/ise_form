<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/trainings")
 */
class AdminTrainingsController extends Controller
{
    /**
     * @Route("/", name="admin_trainings_index")
     */
    public function indexAction()
    {
    }

    /**
     * @Route("/create", name="admin_trainings_create")
     */
    public function createAction(Request $request)
    {
    }

    /**
     * @Route("/read", name="admin_trainings_read")
     */
    public function readAction()
    {
    }

    /**
     * @Route("/update", name="admin_trainings_update")
     */
    public function updateAction(Request $request)
    {
    }

    /**
     * @Route("/delete", name="admin_trainings_delete")
     */
    public function deleteAction(Request $request)
    {
    }

    /**
     * @Route("/{id}/edit", name="admin_trainings_edit")
     */
    public function editAction($id)
    {
    }
}
