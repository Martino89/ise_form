<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/courses")
 */
class AdminCoursesController extends Controller
{
    /**
     * @Route("/", name="admin_courses_index")
     */
    public function indexAction()
    {
    }

    /**
     * @Route("/create", name="admin_courses_create")
     */
    public function createAction(Request $request)
    {
    }

    /**
     * @Route("/read", name="admin_courses_read")
     */
    public function readAction()
    {
    }

    /**
     * @Route("/update", name="admin_courses_update")
     */
    public function updateAction(Request $request)
    {
    }

    /**
     * @Route("/delete", name="admin_courses_delete")
     */
    public function deleteAction(Request $request)
    {
    }

    /**
     * @Route("/{id}/edit", name="admin_courses_edit")
     */
    public function editAction($id)
    {
    }
}
