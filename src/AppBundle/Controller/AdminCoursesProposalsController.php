<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/courses/proposals")
 */
class AdminCoursesProposalsController extends Controller
{
    /**
     * @Route("/", name="admin_courses_proposals_index")
     */
    public function indexAction()
    {
        return $this->render('admin/coursesProposals.html.twig');
    }

    /**
     * @Route("/get/all", name="admin_courses_proposals_get_all", options = { "expose" = true })
     */
    public function getAllAction()
    {
        $result = $this->get('AppBundle.courses_proposals_query_srv')->getAll();
        dump($result);
        return new JsonResponse($result);
    }

    /**
     * @Route("/delete", name="admin_courses_proposals_delete", options = { "expose" = true })
     */
    public function deleteAction(Request $request)
    {
        $id = $request->request->get('element');
        $delete = $this->get('AppBundle.courses_proposals_cmd_srv')->deleteCourseProposalById($id);
        return new Response('usuniete');
    }

}
