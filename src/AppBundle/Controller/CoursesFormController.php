<?php
/**
 * Created by PhpStorm.
 * User: mjendrasik
 * Date: 2017-04-06
 * Time: 16:00
 */

namespace AppBundle\Controller;

use AppBundle\Entity\IseCoprsSubs;
use AppBundle\Entity\IseSubjects;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\IseCourseProposalsType;
use AppBundle\Entity\IseCourseProposals;

class CoursesFormController extends Controller
{

    /**
     * @Route("/course/form/", name="course_form")
     */
    public function indexAction(Request $request)
    {



        $courses_type = $this->get('appbundle.courses_proposals_query_srv')->getType();
        $subjects_list = $this->get('appbundle.courses_proposals_query_srv')->getSubjects();
        $methods  = $this->get('appbundle.courses_proposals_query_srv')->getMethods();
        $qualifications  = $this->get('appbundle.courses_proposals_query_srv')->getQualifications();
        $degree  = $this->get('appbundle.courses_proposals_query_srv')->getDegree();

        $courseProposal = new IseCourseProposals();
        $subjects = new IseSubjects();
        $subjects_proposals = new IseCoprsSubs();
        $courseProposal->getSubjects()->add($subjects);

        $form  = $this->createForm(IseCourseProposalsType::class, $courseProposal,[
            'subjects_list' =>$subjects_list,
            'courses_type' =>$courses_type,
            'methods' => $methods,
            'qualifications' => $qualifications,
            'degree' => $degree,
            ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $this->get('appbundle.courses_proposals_cmd_srv')->saveCourseProposal($form, $courseProposal, $subjects_proposals);

        }
        return $this->render('courses/courseForm.html.twig', [
            'form' => $form->createView(),
        ]);
    }



}