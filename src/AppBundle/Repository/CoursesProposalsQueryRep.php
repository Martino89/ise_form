<?php

namespace AppBundle\Repository;

use Doctrine\DBAL\Connection;

class CoursesProposalsQueryRep
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function getAllCoursesProposals()
    {
        $querySetLang = "SET lc_time_names = 'pl_PL'";
        $stmt = $this->conn->prepare($querySetLang);
        $stmt->execute();

        $query = 'SELECT DATE_FORMAT(FROM_UNIXTIME(icp.copr_date),\'%W, %d %M %Y o %H:%i\') AS proper_date, icp.*, rfk.value AS rfk, sk.value AS sk, kw.value AS kw, sr.value AS sr 
                  FROM ise_course_proposals AS icp 
                  LEFT JOIN (SELECT dv.value, dv.id FROM dict_values AS dv LEFT JOIN dict AS d
                              ON dv.dict_id = d.id WHERE d.alias = "rodzaje-form-ksztalcenia") AS rfk
                  ON icp.cot_id = rfk.id
                  LEFT JOIN (SELECT dv.value, dv.id FROM dict_values AS dv LEFT JOIN dict AS d
                              ON dv.dict_id = d.id WHERE d.alias = "stopnie-ksztalcenia") AS sk
                  ON icp.col_id = sk.id
                  LEFT JOIN (SELECT dv.value, dv.id FROM dict_values AS dv LEFT JOIN dict AS d
                              ON dv.dict_id = d.id WHERE d.alias = "kwalifikacje") AS kw
                  ON icp.coq_id = kw.id
                  LEFT JOIN (SELECT dv.value, dv.id FROM dict_values AS dv LEFT JOIN dict AS d
                              ON dv.dict_id = d.id WHERE d.alias = "sposoby-rekrutacji") AS sr
                  ON icp.coa_id = sr.id';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();
        return $results;
    }

    public function getSubjectsByProposalId($proposal_id)
    {
        $query = 'SELECT dv.value 
        FROM dict_values AS dv
        LEFT JOIN dict AS d 
        ON d.id = dv.dict_id
        LEFT JOIN ise_coprs_subs AS ics 
        ON ics.sub_id = dv.id
        LEFT JOIN ise_course_proposals AS icp 
        ON ics.copr_id = icp.copr_id
        WHERE d.alias = "przedmioty" AND icp.copr_id = ' . $proposal_id;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();
        return $results;
    }
}