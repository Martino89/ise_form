<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Connection;

class TrainingsProposalsQueryRep
{
    private $em;
    private $conn;

    public function __construct(EntityManager $em,Connection $conn)
    {
        $this->em = $em;
        $this->conn = $conn;
    }

    public function getAllTrainingsProposals()
    {
        $querySetLang = "SET lc_time_names = 'pl_PL'";
        $stmt = $this->conn->prepare($querySetLang);
        $stmt->execute();

        $query = "SELECT ise_training_proposals.trp_id,ise_training_proposals.trp_fac_name,ise_training_proposals.trp_fac_town,ise_training_proposals.trp_fac_address,ise_training_proposals.trp_aut_email,ise_training_proposals.trp_aut_name, DATE_FORMAT(FROM_UNIXTIME(ise_training_proposals.trp_date),'%W, %d %M %Y o %H:%i') as `trp_date`,ise_training_proposals.trp_tra_name,ise_training_proposals.trt_id,ise_training_proposals.trp_tra_schoolyear,ise_training_proposals.trp_tra_hours,ise_training_proposals.trp_tra_description,dict_values.value FROM `ise_training_proposals` JOIN dict_values ON ise_training_proposals.trt_id = dict_values.id JOIN dict on dict_values.dict_id = dict.id AND dict.alias = 'rodzaje-form-doskonalenia' ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();
        return $results;
    }

    public function getSubjectsByTrainingsProposalsId($id)
    {
        $query = 'SELECT * FROM `ise_training_proposals` JOIN ise_trps_subs ON ise_training_proposals.trp_id=ise_trps_subs.trp_id
                 JOIN dict_values ON ise_trps_subs.sub_id = dict_values.id JOIN
                  dict ON dict_values.dict_id = dict.id AND dict.alias = \'przedmioty\' and ise_training_proposals.trp_id = :id   ';

        $stmt = $this->conn->prepare($query);
        $params = [
            'id' => $id
        ];
        $stmt->execute($params);
        $results = $stmt->fetchAll();
        return $results;
    }
}