<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TrainingsProposalsCmdRep extends Controller
{

    private $em;
    /**
     * TrainingsProposalsCmdRep constructor.
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function saveTrainingsProposal($IseTrainingProposals, $form, $IseTrpsSubs)
    {
        $em = $this->em;
        $IseTrainingProposals->setTrpDate(time());
        $em->persist($IseTrainingProposals);
        $em->flush();

        foreach ($form->getData()->getSubject()[0]->getSubName() as $item) {
            $IseTrpsSubs->setTrpId($IseTrainingProposals->getTrpId());
            $IseTrpsSubs->setSubId($item);
            $em->persist($IseTrpsSubs);
            $em->flush();
            $em->clear();
        }
    }

    public function deleteTrainingProposalById($id)
    {
        $proposals = $this->em->getRepository('AppBundle:IseTrainingProposals');
        $result = $proposals->findOneByTrpId($id);
        $this->em->remove($result);
        $this->em->flush();
    }
}