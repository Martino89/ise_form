<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CoursesProposalsCmdRep
{
    private $em;

    /**
     * CoursesProposalsCmdRep constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function deleteCourseProposalById($id)
    {
        $proposals = $this->em->getRepository('AppBundle:IseCourseProposals');
        $result = $proposals->findOneByCoprId($id);
        $this->em->remove($result);
        $this->em->flush();
        return $result;
    }

    public function saveCourseProposal($form, $courseProposal, $subjects_proposals)
    {
        $data = $form->getData();
        $courseProposal->setCoprDate(time());

        $this->em->persist($data);
        $this->em->flush();
        $this->em->clear();

        $proposal_id = $courseProposal->getCoprId();

        foreach ($data->getSubjects()[0]->getSubName() as $item) {
            $subjects_proposals->setCoprId($proposal_id);
            $subjects_proposals->setSubId($item);
            $this->em->persist($subjects_proposals);
            $this->em->flush();
            $this->em->clear();
        }
    }
}