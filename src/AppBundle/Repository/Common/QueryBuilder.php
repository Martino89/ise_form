<?php
/**
 * Created by PhpStorm.
 * User: mbiegalski
 * Date: 2017-04-13
 * Time: 10:35
 */

namespace AppBundle\Repository\Common;


trait QueryBuilder
{
    private $where_clause;
    private $conditions_values;

    public function andWhere($column, $value, $type)
    {
        if(!$value)
        {
            return;
        }
        $condition = $type . 'Where';

        $this->conditions_values[] = $this->$condition($column, $value);

    }

    public function buildWhereFilter()
    {
        if(empty($this->conditions_values))
        {
            return;
        }
        $filter = 'WHERE ';

        $filter .= implode(' AND ', $this->conditions_values);

        return $this->where_clause = $filter;
    }


    public function buildQueryWithConditions($query)
    {
        return $query . ' ' . $this->where_clause;
    }

    public function orElements($column, $array)
    {
        if(!empty($array))
        {
            $orCond = $this->orWhereEquals($column, $array);

            $filter = implode(' OR ', $orCond);

            $this->conditions_values[] = ' ( '.$filter.' )';
        }

    }

    private function orWhereEquals($column, $array)
    {
        $conditions_or =[];
        foreach ($array as $item)
        {
            $conditions_or[] = " $column  =  $item ";
        }

        return $conditions_or;
    }



    private function likeWhere($column, $value)
    {
        $like = " $column  LIKE '%$value%' ";
        return $like;
    }

    private function equalsWhere($column, $value)
    {
        $equals = " $column = $value ";
        return $equals;
    }
}