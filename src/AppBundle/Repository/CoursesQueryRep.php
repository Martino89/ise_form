<?php

/**
 * Created by PhpStorm.
 * User: mbiegalski
 * Date: 2017-04-06
 * Time: 16:17
 */

namespace AppBundle\Repository;


use AppBundle\Repository\Common\QueryBuilder;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use PDO;



class CoursesQueryRep
{
    use QueryBuilder;
    private $em;
    private $conn;

    /**
     * CoursesQueryRep constructor.
     * @param $em
     */
    public function __construct(EntityManager $em, Connection $conn)
    {
        $this->em = $em;
        $this->conn = $conn;
    }


    public function get($filter = null, $offset, $limit)
    {
        $result = $this->filterQuery($filter,$offset,$limit);
        return $result;
    }

    public function filterQuery($filter,$offset,$limit)
    {

        $query = "SELECT
  IC.cou_id, IC.cou_name course_name, DICT1.value qual, DICT2.value levels, DICT3.value course_types, DICT4.value appl, DICT5.value subjects, DICT6.value  province,
  IC.cou_semesters semesters, ISEF.*, parent.*
FROM
  ise_courses AS IC
JOIN (SELECT dict.alias as alias, dict_values.id as id, dict_values.value as value FROM dict, dict_values WHERE dict.id = dict_values.dict_id)	DICT1
ON DICT1.id = IC.coq_id AND DICT1.alias = 'kwalifikacje'
JOIN (SELECT dict.alias as alias, dict_values.id as id, dict_values.value as value FROM dict, dict_values WHERE dict.id = dict_values.dict_id)	DICT2
ON DICT2.id = IC.col_id AND DICT2.alias = 'stopnie-ksztalcenia'
JOIN (SELECT dict.alias as alias, dict_values.id as id, dict_values.value as value FROM dict, dict_values WHERE dict.id = dict_values.dict_id)	DICT3
ON DICT3.id = IC.cot_id AND DICT3.alias = 'rodzaje-form-ksztalcenia'
JOIN (SELECT dict.alias as alias, dict_values.id as id, dict_values.value as value FROM dict, dict_values WHERE dict.id = dict_values.dict_id)	DICT4
ON DICT4.id = IC.coa_id AND DICT4.alias = 'sposoby-rekrutacji'
JOIN (SELECT ICS.cou_id as id, DICT.value as value, DICT.id as sub_id FROM ise_cous_subs as ICS
		JOIN (SELECT dict.alias as alias, dict_values.id as id, dict_values.value as value 
        FROM dict, dict_values WHERE DICT.id = dict_values.dict_id)	DICT
		ON DICT.id = ICS.sub_id AND DICT.alias = 'przedmioty') DICT5
ON DICT5.id = IC.cou_id
JOIN ise_facilities as ISEF
ON ISEF.fac_id = IC.fac_id
LEFT JOIN (SELECT IceF.fac_id, IceF.fac_name as parent_name, IceF.fac_town   as parent_town,
           IceF.fac_address as parent_adress, IceF.fac_phone  as parent_phone, IceF.fac_fax as parent_fax,
           IceF.fac_www  as parent_www, IceF.fac_description  as parent_desc,
           IceF.fac_postcode as parent_postcode FROM ise_facilities as IceF) parent
ON ISEF.parent_fac_id = parent.fac_id

JOIN (SELECT dict.alias as alias, dict_values.id as id, dict_values.value as value FROM dict, dict_values WHERE dict.id = dict_values.dict_id)	DICT6
ON DICT6.id = ISEF.pro_id AND DICT6.alias = 'wojewodztwa'";


        if($filter) {

            $this->andWhere('DICT6.id', $filter['selected_voiv'], 'equals');
            $this->andWhere('IC.col_id', $filter['selected_degree'], 'equals');
            $this->andWhere('IC.coq_id', $filter['selected_qualification'], 'equals');
            $this->andWhere('ISEF.fac_town ', $filter['city'], 'like');
            $this->andWhere('ISEF.fac_name', $filter['school_name'], 'like');
            $this->andWhere('IC.cou_name', $filter['study_name'], 'like');

            if (array_key_exists('checked_subjects', $filter)) {
                $this->orElements('DICT5.sub_id', $filter['checked_subjects']);
            }
            $this->buildWhereFilter();
            $query = $this->buildQueryWithConditions($query);

        }

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);

        $quantity =sizeof($result);
        $pages=(int)ceil($quantity/$limit);
        $result = array_slice($result, $offset,$limit, true);


        return [
            'result' => $result,
            'quantity' => $quantity,
            'pages' => $pages
        ];

    }



}