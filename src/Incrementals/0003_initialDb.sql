-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Kwi 2017, 10:17
-- Wersja serwera: 10.1.21-MariaDB
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ise`
--
DROP TABLE IF EXISTS dict, dict_values;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dict`
--

CREATE TABLE `dict` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dict`
--

INSERT INTO `dict` (`id`, `alias`, `name`) VALUES
(1, 'sposoby-rekrutacji', 'Sposoby rekrutacji'),
(2, 'stopnie-ksztalcenia', 'Stopnie kształcenia'),
(3, 'kwalifikacje', 'Kwalifikacje'),
(4, 'rodzaje-form-ksztalcenia', 'Rodzaje form kształcenia'),
(5, 'typy-placowek', 'Typy placówek'),
(6, 'wojewodztwa', 'Województwa'),
(7, 'przedmioty', 'Przedmioty'),
(8, 'rodzaje-form-doskonalenia', 'Rodzaje form doskonalenia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dict_values`
--

CREATE TABLE `dict_values` (
  `id` int(11) NOT NULL,
  `dict_id` int(11) NOT NULL,
  `value` varchar(512) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dict_values`
--

INSERT INTO `dict_values` (`id`, `dict_id`, `value`) VALUES
(1, 1, 'egzamin pisemny'),
(1, 2, 'studia magisterskie'),
(1, 3, 'licencjat'),
(1, 4, 'stacjonarne'),
(1, 5, 'przedszkola'),
(1, 7, 'biologia'),
(1, 8, 'kurs doskonalący'),
(2, 1, 'egzamin ustny'),
(2, 2, 'studia inżynierskie'),
(2, 3, 'inżynierskie'),
(2, 4, 'zaoczne'),
(2, 5, 'szkoły podstawowe'),
(2, 6, 'dolnośląskie'),
(2, 7, 'chemia'),
(2, 8, 'konferencja'),
(3, 1, 'egzamin praktyczny'),
(3, 2, 'studia I stopnia'),
(3, 3, 'magisterium'),
(3, 4, 'wieczorowe'),
(3, 5, 'zasadnicze szkoły zawodowe'),
(3, 7, 'fizyka i astronomia'),
(3, 8, 'sympozjum'),
(4, 1, 'rozmowa kwalifikacyjna'),
(4, 3, 'doktorat'),
(4, 4, 'eksternistyczne'),
(4, 5, 'punkty przedszkolne'),
(4, 6, 'kujawsko-pomorskie'),
(4, 7, 'geografia'),
(4, 8, 'studium (podyplomowe)'),
(5, 1, 'konkurs świadectw'),
(5, 2, 'studia 3 - letnie po SN'),
(5, 3, 'dodatkowa specjalność'),
(5, 4, 'kształcenie na odległość'),
(5, 5, 'licea ogólnokształcące'),
(5, 7, 'historia'),
(5, 8, 'seminarium'),
(6, 1, 'kolejność zgłoszeń'),
(6, 2, 'studia uzupełniające 1,5 - 2 - letnie licencjackie po SN'),
(6, 3, 'doskonalenie'),
(6, 6, 'lubelskie'),
(6, 7, 'informatyka'),
(6, 8, 'sesja'),
(7, 1, 'konkurs dyplomów'),
(7, 2, 'studia uzupełniające magisterskie poinżynierskie'),
(7, 3, 'inne'),
(7, 5, 'technika'),
(7, 7, 'język angielski (filologia angielska)'),
(7, 8, 'samokształcenie kierowane'),
(8, 1, 'egzamin pisemny i ustny'),
(8, 2, 'studia podyplomowe - doskonalące'),
(8, 5, 'technika uzupełniające dla absolwentów zasadniczych szkół zawodowych'),
(8, 6, 'lubuskie'),
(8, 7, 'język francuski (filologia romańska)'),
(8, 8, 'zjazd'),
(9, 1, 'egzamin pisemny i rozmowa kwalifikacyjna'),
(9, 2, 'studia podyplomowe - dodatkowa specjalność'),
(9, 5, 'szkoły policealne (ponadgimnazjalne)'),
(9, 7, 'język niemiecki (filologia germańska)'),
(9, 8, 'inne'),
(10, 1, 'egzamin praktyczny i ustny'),
(10, 2, 'doktoranckie'),
(10, 5, 'czteroletnie licea plastyczne'),
(10, 6, 'łódzkie'),
(10, 7, 'język rosyjski (filologia rosyjska)'),
(10, 8, 'kurs kwalifikacyjny'),
(11, 1, 'egzamin pisemny, ustny, praktyczny i rozmowa kwalifikacyjna'),
(11, 2, 'studia II stopnia'),
(11, 5, 'dziewięcioletnie ogólnokształcące szkoły baletowe'),
(11, 7, 'inne języki obce (inne filologie)'),
(11, 8, 'warsztaty'),
(12, 1, 'egzamin praktyczny i rozmowa kwalifikacyjna'),
(12, 5, 'sześcioletnie szkoły sztuki tańca'),
(12, 6, 'małopolskie'),
(12, 7, 'język polski (filologia polska)'),
(12, 8, 'kurs edukatorski'),
(13, 1, 'konkurs świadectw i rozmowa kwalifikacyjna'),
(13, 5, 'bursy'),
(13, 7, 'matematyka'),
(14, 1, 'egzamin pisemny, konkurs świadectw i rozmowa kwalifikacyjna'),
(14, 5, 'czteroletnia szkoła sztuki cyrkowej'),
(14, 6, 'mazowieckie'),
(14, 7, 'wychowanie muzyczne'),
(14, 8, 'kurs'),
(15, 1, 'egzamin praktyczny i konkurs świadectw'),
(15, 5, 'specjalne ośrodki szkolno-wychowawcze'),
(15, 7, 'plastyka'),
(15, 8, 'kurs e-learningowy'),
(16, 5, 'zespoły wychowania przedszkolnego'),
(16, 6, 'opolskie'),
(16, 7, 'technika (wychowanie techniczne)'),
(16, 8, 'konsultacje'),
(17, 1, 'egzamin pisemny, ustny, rozmowa kwalifikacyjna'),
(17, 5, 'specjalne ośrodki wychowawcze'),
(17, 7, 'wychowanie fizyczne'),
(17, 8, 'kurs nadający uprawnienia'),
(18, 1, 'egzamin pisemny, praktyczny, rozmowa kwalifikacyjna'),
(18, 6, 'podkarpackie'),
(18, 7, 'edukacja wczesnoszkolna'),
(18, 8, 'szkolenie'),
(19, 1, 'egzamin pisemny i konkurs świadectw'),
(19, 5, 'kolegia nauczycielskie'),
(19, 7, 'wychowanie przedszkolne'),
(19, 8, 'konkurs'),
(20, 1, 'na stronie internetowej'),
(20, 5, 'nauczycielskie kolegia języków obcych'),
(20, 6, 'podlaskie'),
(20, 7, 'pedagogika opiekuńczo-wychowawcza'),
(20, 8, 'grant'),
(21, 1, '.'),
(21, 5, 'placówki doskonalenia nauczycieli publiczne gminne i powiatowe'),
(21, 7, 'pedagogika specjalna'),
(21, 8, 'szkolenia rady pedagogicznej'),
(22, 1, 'informacja u organizatora'),
(22, 5, 'kuratorium oświaty z oddziałami'),
(22, 6, 'pomorskie'),
(22, 7, 'pedagogiki pozostałe'),
(22, 8, 'sieć współpracy'),
(23, 1, 'średnia wszystkich ocen ze studiów I stopnia'),
(23, 5, 'biblioteki pedagogiczne z filiami'),
(23, 7, 'psychologia'),
(24, 5, 'poradnie psychologiczno-pedagogiczne publiczne'),
(24, 6, 'śląskie'),
(24, 7, 'logopedia'),
(25, 5, 'centra kształcenia ustawicznego ze szkołami'),
(25, 7, 'bibliotekoznawstwo i INTE'),
(26, 5, 'młodzieżowe ośrodki wychowawcze'),
(26, 6, 'świętokrzyskie'),
(26, 7, 'inne ogólnokształcące'),
(27, 5, 'licea ogólnokształcące uzupełniające dla absolwentów zasadniczych szkół zawodowych'),
(27, 7, 'artystyczny'),
(28, 5, 'młodzieżowe ośrodki socjoterapii ze szkołami'),
(28, 6, 'warmińsko-mazurskie'),
(29, 5, 'gimnazja'),
(30, 5, 'centra kształcenia praktycznego'),
(30, 6, 'wielkopolskie'),
(30, 7, 'drzewny, leśny'),
(31, 5, 'wydziały szkół wyzszych'),
(31, 7, 'ekonomia'),
(32, 5, 'instytuty i katedry szkół wyższych'),
(32, 6, 'zachodniopomorskie'),
(32, 7, 'elektroniczny'),
(33, 5, 'szkoły wyższe'),
(33, 7, 'elektryczny'),
(34, 7, 'gastronomiczny'),
(35, 5, 'placówki doskonalenia nauczycieli niepubliczne'),
(36, 5, 'młodzieżowe ośrodki socjoterapii bez szkół'),
(39, 5, 'licea profilowane'),
(39, 7, 'muzyka'),
(40, 5, 'placówki doskonalenia nauczycieli publiczne wojewódzkie'),
(41, 5, 'komisje egzaminacyjne'),
(42, 5, 'jednostki samorządu terytorialnego'),
(43, 5, 'placówki doskonalenia nauczycieli centralne'),
(44, 5, 'Poradnie psychologiczno-pedagogiczne publiczne specjalistyczne'),
(44, 7, 'inne zawody'),
(45, 5, 'poradnie psychologiczno-pedagogiczne niepubliczne'),
(45, 7, 'edukacja obronna'),
(46, 7, 'politologia'),
(47, 5, 'stowarzyszenia nauczycielskie'),
(47, 7, 'filozofia'),
(48, 7, 'administracja'),
(49, 7, 'edukacja psychologiczna'),
(50, 7, 'teologia'),
(51, 7, 'inżynieria środowiska'),
(52, 7, 'język biznesu'),
(53, 7, 'kulturoznawstwo'),
(54, 7, 'socjologia'),
(55, 7, 'wychowanie seksualne'),
(56, 7, 'promocja zdrowia'),
(57, 7, 'przedmioty zawodowe'),
(58, 7, 'innowacja pedagogiczna'),
(59, 7, 'zarządzanie oświatą (organizacja)'),
(60, 7, 'pomiar dydaktyczny'),
(61, 7, 'ewaluacja'),
(62, 7, 'edukacja ekologiczna (ochrona środowiska)'),
(63, 7, 'wychowanie'),
(64, 7, 'metodyka nauczania'),
(65, 7, 'pedagogika ogólna'),
(66, 7, 'integracja przedmiotowa'),
(67, 7, 'ocenianie'),
(68, 7, 'zmiana'),
(69, 7, 'reforma oświaty'),
(70, 7, 'języki słowiańskie (filologia słowiańska)'),
(71, 7, 'język hiszpański (filologia hiszpańska)'),
(72, 7, 'język włoski'),
(73, 7, 'przyroda'),
(74, 7, 'pedagogika resocjalizacyjna'),
(75, 7, 'zarządzanie jakością'),
(76, 7, 'nauczanie na odległość'),
(77, 7, 'zarządzanie i marketing'),
(78, 7, 'fizyka'),
(79, 7, 'fizyka z informatyką'),
(80, 7, 'fizyka z matematyką'),
(81, 7, 'matematyka z informatyką'),
(82, 7, 'prawo'),
(83, 7, 'pedagogika pracy'),
(84, 7, 'pedagogika kulturalna'),
(85, 7, 'pedagogika społeczna'),
(86, 7, 'historia sztuki'),
(87, 7, 'stosunki międzynarodowe'),
(88, 7, 'wiedza o społeczeństwie'),
(89, 7, 'religia'),
(90, 7, 'pedagogika lecznicza'),
(91, 7, 'praktyczne nauczanie zawodu'),
(92, 7, 'kształcenie zintegrowane'),
(93, 7, 'doradztwo zawodowe'),
(94, 7, 'inne'),
(95, 7, 'animacja społeczno-kulturalna'),
(96, 7, 'terapia pedagogiczna (reedukacja)'),
(97, 7, 'archeologia'),
(98, 7, 'oświata dorosłych'),
(99, 7, 'etnologia'),
(100, 7, 'integracja europejska'),
(101, 7, 'gimnastyka korekcyjna'),
(103, 7, 'kwalifikacje pedagogiczne'),
(104, 7, 'wychowanie do życia w rodzinie'),
(105, 7, 'wewnątrzszkolne doskonalenie nauczycieli (WDN)'),
(106, 7, 'mierzenie jakości pracy szkoły'),
(107, 7, 'programy nauczania'),
(108, 7, 'edukacja regionalna'),
(109, 7, 'kształcenie blokowe'),
(110, 7, 'aktywizujące metody nauczania'),
(111, 7, 'dziennikarstwo'),
(112, 7, 'turystyka'),
(113, 7, 'energetyczny'),
(114, 7, 'hotelarski'),
(116, 7, 'meneger oświaty'),
(117, 7, 'przedsiębiorczość'),
(118, 7, 'socjoterapia'),
(119, 7, 'sztuka'),
(120, 7, 'awans zawodowy nauczyciela'),
(121, 7, 'samorząd terytorialny'),
(122, 7, 'jakość'),
(123, 7, 'edukacja europejska'),
(124, 7, 'public relations'),
(125, 7, 'asertywność'),
(126, 7, 'komunikacja interpersonalna'),
(127, 7, 'ścieżki międzyprzedmiotowe'),
(128, 7, 'edukacja obywatelska'),
(129, 7, 'edukacja czytelnicza i medialna'),
(130, 7, 'bankowość i finanse'),
(131, 7, 'rachunkowość'),
(133, 7, 'organizacja i zarządzanie'),
(134, 7, 'Praca socjalna'),
(135, 7, 'wychowawca placówek wypoczynku'),
(136, 7, 'emisja głosu'),
(137, 7, 'doskonalenie nauczycieli'),
(138, 7, 'warsztat pracy nauczyciela'),
(139, 7, 'profilaktyka uzależnień'),
(140, 7, 'języki obce'),
(143, 7, 'wiedza o kulturze'),
(144, 7, 'hospitacja'),
(145, 7, 'przysposobienie obronne'),
(147, 7, 'edukator'),
(148, 7, 'techniki uczenia się'),
(149, 7, 'nadzór pedagogiczny'),
(150, 7, 'metoda projektu'),
(151, 7, 'BHP'),
(152, 7, 'matura'),
(153, 7, 'andragogika'),
(154, 7, 'kierownik wycieczek szkolnych'),
(155, 7, 'mechatronika'),
(156, 7, 'rodzice'),
(157, 7, 'wychowanie do życia w społeczeństwie'),
(158, 7, 'wychowanie patriotyczne i obywatelskie'),
(159, 7, 'kształcenie integracyjne'),
(160, 7, 'integracja'),
(161, 7, 'edukacja prozdrowotna'),
(162, 7, 'etyka'),
(163, 7, 'edukacja filozoficzna'),
(164, 7, 'zarządzanie informacją'),
(165, 7, 'zarządzanie zasobami ludzkimi'),
(166, 7, 'zarządzanie sytuacją kryzysową'),
(167, 7, 'agresja w szkole'),
(168, 7, 'edukacja dla bezpieczeństwa'),
(169, 7, 'pomoc psychologiczno-pedagogiczna'),
(170, 7, 'technologie informacyjno-komunikacyjne'),
(171, 7, 'dydaktyka'),
(172, 7, 'drama'),
(173, 7, 'dysleksja'),
(174, 7, 'specjalne potrzeby edukacyjne'),
(175, 7, 'dziecko zdolne'),
(176, 7, 'lingwistyka stosowana'),
(177, 7, 'wielokulturowość'),
(178, 7, 'przyroda przedmiot uzupełniający'),
(179, 7, 'historia i społeczeństwo');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `dict`
--
ALTER TABLE `dict`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `dict_values`
--
ALTER TABLE `dict_values`
  ADD PRIMARY KEY (`id`,`dict_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dict`
--
ALTER TABLE `dict`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `dict_values`
--
ALTER TABLE `dict_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
